# Recherche-Stammdaten

Hier werden die gesammelten Daten für die Projektplanung in einer Tabelle aufgelistet. Darüber hinaus wurden die Daten auch auf die gesamte Breite des Themas erweitert. Um die Umfrage auch regelkonform zu gestalten, wurden ein Datenschutz-Disclaimer und ein Rechtshinweis hinzugefügt!

  

Hier auch als Word-Dok. und PDF:

[Planungsfragebogen.docx](https://t43259044.p.clickup-attachments.com/t43259044/34c11f70-777f-4705-8cca-50c4fae4cc0e/Planungsfragebogen.docx)

[Planungsfragebogen\_Stammdatenblatt.pdf](https://t43259044.p.clickup-attachments.com/t43259044/0a3bce6a-ac68-4e22-9b2c-960a42866bf3/Planungsfragebogen_Stammdatenblatt.pdf)

  

  

  

| <br>Projektdaten<br> |
| --- |
| <br>Einsender Kontaktperson<br> | <br>Daten für Bauvorhaben (Bei Abweichungen zum Einsender)<br> |
| Nachname & Vorname |  | Bauherr / Projekt |  |
| Straße, Nr. |  | Ansprechpartner |  |
| PLZ, Ort |  | Telefon |  |
| Land |  | E-Mail |  |
| E-Mail |  | Straße, Nr. d. Bauortes |  |
| Telefon |  | PLZ, Bauort |  |
| <br>Gewünschte Dienstleistung<br> | <br>Gewünschte PV- Anlagengröße<br> |
| Nur Material |  Panele  Wechselrichter  Speicher | Modulleistung (kWp) |  |
|  Montagematerial | Wechselrichter (kW) |  |
|  Inkl. Anschlussarbeiten |  Elektriker  Montage | Stromspeicher (kWh) |  |
| Komplettanlage |  Material, Elektriker und Montage |  |  |
| <br>Stromanschluss-Daten<br> |
| Stromverbrauch \[kWh/Jahr\] |  | Inv. Nr. ([003.XXX.XXX](http://003.XXX.XXX))\*\* |  |
| Kundennummer Netzbetreiber |  | Einspeisezählpunkt |  Vorhanden  Beantragt  Nein |
| Stromnetzbetreiber |  |
| Zählpunktnummer\*\* |  |
|  |  |  |  |  |

\*\* Inv. Nr.  ([003.XXX.XXX](http://003.XXX.XXX))

ist am Stromzähler zu finden   \*\* Zählpunktnummer ist eine 33-stellige Nummer und auf der Stromrechnung zu finden

  

| <br>Gebäudedaten<br> |
| --- |
| <br>Gebäude Pläne und Gebäude Nutzung<br> |
|  | Aktuelle bemaße Gebäudepläne liegen bei |  | Neubau/Rohbau |
|  | Wohnhaus |  | Altbau, Baujahr: |
|  | Landwirtschaftliches Gebäude |  | Exponierte Lage (umgeben von Bergen, Felsen starker Wind) |
|  | Industriegebäude |  | Denkmalschutz |
| <br>Gebäude- und Dachflächen Ausrichtung<br> |
|  | Dachflächenausrichtung (Standard = Süd) |  |
|  |
| <br>Dach Art<br> |
|  | Schrägdach |  | Zelt-/Walmdach |
|  | Flachdach |  | Sheddach |
|  | Satteldach |  | Sonstiges: |
| <br>Dach Eindeckung<br> |
|  | Ziegel |  | Biberschwanz |
|  | Welleternit |  | Blechfalz |
|  | Platteneternit |  | Trapezblech, Blechstärke (in mm): |
|  | Sandwichplatte |  | Sonstige: |
| <br>Dach Konstruktion<br> |
|  | Aus Holz |  | Sparrenabstand (inmm) (Standard = 700 mm) |
|  | Aus Metall |  | Pfetten Abstand (in mm) |
|  | Dachlattenbreite (in mm) (Standard = 48 mm) |  | Dachlattenstärke (in mm) (Standard = 24 mm) |
|  | Sparren-/Pfetten breite (in mm) (Standard = 80 mm) |  | Sparren-/Pfetten Höhe (in mm) (Standard = 200 mm) |
|  | x (in m) Ortgang | y<br>x<br>H2<br>a<br>H1<br>_H1 = Höhe der Traufkante_<br>_H1 + H2 = Höhe des Firstes_<br>_a  = Neigungswinkel des Daches_ |
|  | y (in m) Firstlänge |
|  | Höhe der Traufkante bzw. Regenrinne (in m; H1 in m) |
|  | Höhe desFirstes (in m; H1+H2 in m) (Standard = 8 m) |
|  | Neigungswinkel des Daches (in <°) (Standard = 30°) |
|  |  |
|  |  |
| <br><br>Anlageninstallation<br> |
|  | Auf Dach |  | Modulausrichtung hochkant (optional) |
|  | Aufständerung |  | Modulausrichtung quer (optional) |
|  |  |  |  |
| <br>Auf Dach Objekte (Objekte bitte in die Skizze schematisch eintragen)<br> |
|  | störflächenfrei |  | Schneefanggitter |
|  | Schornstein(e) |  | Solarthermie |
|  | Gaube(n) |  | Äußerer Blitzschutz |
|  | Dachfenster |  | Sonstiges: |
| <br>Umgebung (Objekte bitte in der Skizze schematisch eintragen)<br> |
|  | verschattungsfrei |  | Oberleitung |
|  | Vegetation (Bäume, Pflanzen,  ..) |  | Freie Baugrundstücke |
|  | Gebäude |  | Sonstiges: |
|  | Mast(en) |  |  |
| <br>Wechselrichter<br> |
|  | Leitungslänge Modulfeld-Wechselrichter (in m) (Standard = 25 m) |
|  | Leitungslänge Wechselrichter-Zähler (in m) |
|  | Anzahl freier Zählerplätze im Schaltschrank |
|  |  |
| <br>Zusätzliche Leistungen<br> |
|  | Anlagenüberwachung (Smart Meter, ..) |  |  |
|  |  |  |  |

  

  

Planungsfragebogen drucken und versenden Weitere Bau-Unterlagen

Je detaillierter und genauer Sie uns Ihre zu planende Anlage bzw. das Gebäude

beschreiben, desto genauer können wir für Sie planen. Sofern Sie über weitere Gebäude-

Zeichnungen bzw. Fotos und eine Baubeschreibung verfügen, lassen Sie uns diese

Unterlagen bitte vollständig in Kopie zukommen.

Digital

Unterschreiben

Oder Drucken Sie Ihren Planungsfragebogen aus und senden Sie

ihn unterschrieben an die obenstehende E-Mail.

Datenschutzerklärung

Sie haben uns Daten über sich freiwillig zur Verfügung gestellt und wir verarbeiten

diese Daten auf Grundlage Ihrer Einwilligung zu folgenden Zwecken:

• Betreuung des Kunden sowie

• für eigene Werbezwecke, beispielsweise zur Zusendung von

Angeboten, Werbeprospekten und Newsletter (in Papier- und elektronischer

Form), sowie zum Zwecke des Hinweises auf die zum Kunden bestehende oder

  

vormalige Geschäftsbeziehung (Referenzhinweis).

Sie können diese Einwilligung jederzeit widerrufen. Ein Widerruf hat zur Folge,

dass wir Ihre Daten ab diesem Zeitpunkt zu oben genannten Zwecken nicht mehr

  

verarbeiten.

Die von Ihnen bereit gestellten Daten sind weiters zur Vertragserfüllung bzw. zur

Durchführung vorvertraglicher Maßnahmen erforderlich. Ohne diese Daten können

wir den Vertrag mit Ihnen nicht abschließen.

Rechtshinweis

Sie versichern, dass die Angaben vollständig und richtig sind. Sie dienen uns als

Grundlage für die Planung und Kalkulation Ihrer Anlage. Für auf falschen, fehlerhaften

oder nicht vollständigen Angaben beruhenden Berechnungen oder Planungen

übernehmen wir keine Haftung. Falls die von uns erstellte Planung für die Erstellung einer

Anlage eines anderen Herstellers benutzt wird, übernehmen wir keinerlei Haftung oder

Gewährleistung

Datum Unterschrift