# Corporate Identity

1\. OUTER FRAME

  

Leistung

Definition bzw. kurze Beschreibung der Kernleistung der Marke, wie sie im Markt positioniert ist.

Zielgruppe

Definition bzw. kurze Beschreibung der wichtigsten Zielgruppe(n).

Buyer Personae sind hier hilfreich, für B2C-Brands auch Sinus Milieus.

  

2\. INNER FRAME

  

Brand Purpose

Definition bzw. kurze Statement der Existenzberechtigung der Marke. Der Purpose beschreibt den gesellschaftlichen Mehr- wert, den das Unternehmen mit profit- orientierten Aktivitäten stiftet.

Brand Vision

Definition bzw. kurzes Statement, wonach das Unternehmen in der näheren Zukunft strebt. Die Vision orientiert und motiviert Mitarbeiter: innen.

Brand Mission

Definition bzw. kurzes Statement, welchen Mehrwert das Angebot der Marke für Kund: innen stiftet.

Markenwerte

Werte, die Orientierung im täglichen Handeln geben. Wie der Markenkern sollten diese niemals generisch oder ausgedacht sein, sonst verfehlen sie ihre Wirkung.

  

3\. CORE

  

Markenkern

Der Wert, der die Marke maßgeblich prägt und mit dem sie langfristig assoziiert wer- den will. Konsequent in Design, Messaging und kulturell umgesetzt, wird er zum Garant für eine nach innen und außen wahrnehmbare Selbstähnlichkeit. Am Kern wert lässt sich leicht messen, ob eine Botschaft, ein Produkt, oder eine Kampagne der Marke entspricht – oder nicht.

  

  

Die Meeting unterlagen sind zu finden unter: Private ([https://app.clickup.com/t/30rzrwt](https://app.clickup.com/t/30rzrwt))

  

  

Markenslogan: (Markenleitbild)

*   Gemeinsam ermöglichen wir die Energieunabhängigkeit
*   Der Schlüssel zu Ihrer Unabhängigkeit
*   Ihr Schritt zur Energieunabhängigkeit
*   Mit unseren Lösungen in eine Grüne Zukunft 
*   Sorgenfreie grüne unabhängige Zukunft
*   Wir machen Nachhaltigkeit selbstverständlich
*   gemeinsam für Regionale Energieproduktion
*   Wir stehen für regionale Energieproduktion
*   Gemeinsam starten wir die Energieunabhängigkeit
*   Der Energieoptimierer für ihre Unabhängigkeit
*   Wir ermöglichen Energie für Jeder-Mann: Frau.
*   Gemeinsam setzen wir die Energiewende um.
*   Gemeinsam stehen wir für die regionale Stromproduktion Energiewende

  

SolarHYPE: 

  

Markenmantra: [https://www.viversum.de/online-magazin/mantra](https://www.viversum.de/online-magazin/mantra) (Ein Mantra sollte immer POSITIV formuliert sein!)

*   Wenn man die Welt verändern will, muss man bei sich selbst anfangen
*   Bleib in Bewegung
*   Wer denkt, etwas zu sein, hat aufgehört, etwas zu werden
*   Gemeinsam in die Zukunft - Let´s GO
*   In der Ruhe liegt die Kraft
*   Wo ein Wille ist, ist auch ein Weg
*   Wer nicht wagt, der nicht gewinnt

  

  

Leistungen:

  

Dienstleistungen:

*   PV - Komplettanlage (Was benötigt es um eine geschlossene Schleife in der Analgenherstellung zu realisieren?)
    *   Idee Kunde B2B und B2C
    *   Planung & Individuelles Angebot
    *   Förderung und Zählpunktbeantragung
    *   Wirtschaftlichkeitsberechnung Machbarkeit Analyse
    *   Montage und Inbetriebnahme
    *   Service Leistungen (Zukunftsvorschlag Vermittlung?)
        *   Reinigung und Anlagenüberwachung
*   Energycontracting
    *   Energielieferant im Bereich Photovoltaik Contracting
*   Energie Beratung (inkl. Energy Meter )
    *   Energieoptimierer
    *   Dienstleistungen und Betriebskosten zu senken
*   Handel von Nachhaltigen Energiequellen
    *   Wir führen alles rund um Photovoltaik Komplettanlagen PV-Komplettanlagen
        *   Paneel | Wechselrichter | Montagematerial | Speicherbatterie | Zubehör
    *   Weitere Energielösungen

  

Gains:

Zeit

*   Zeitersparnis durch zeitnahe Produktrealisierung  der Komplettanlage
*   Realisierung  und Betreuung Zeitnah
*   Zeitfaktor Beschaffung

Qualität

*   Standardwert ist Qualitätsniveau steigern
*   Planung und Optimierung der Anlage ist die Qualität die wir bieten.

Dynamik

*   Ständiges adaptieren der Produkt Palette möglich - stand der Technik

Netzwerk

*   Klarer Wettbewerbsvorteil wenn wir ein gutes und solides Partnernetzwerk haben

Umwelt/Natur

*   Mehr für den Schutz und erhalt der Natur zu tun
*   Besitz ist ein Statement für eine umweltfreundliche Haltung (schlau)

Energieunabhängigkeit

*   mehr Macht über die Stromkosten zu haben (kosten Ersparnis wegen Energiekrise )
*   Investition in die Zukunft in Bezug auf die Energieerzeugung (Eigenen Strom generieren)
*   Um langfristig kosten einzusparen und Erträge zu Erwirtschaften. Der erste schritt in die Selbstständigkeit und Energie Unabhängigkeit. (Energiekrise und Energieunabhängigkeit )

Für Jedermann/Frau

*   für jedes Budget eine passende Lösung zu liefern
*   individuelle Gestaltung und Anpassung für ihre Bedürfnisse und Geldbörse

  

* * *

Zielgruppe:  (Personae:) ---  Unsere Kunden und Zielgruppe im Überblick:

*   Business und Privat Kunden (Personen Mittleren alters mit Finanzieller Rücklage als Kerngeschäftszielgruppe)
*   Wirtschaftlich denkende Person mit Energiebewusstsein (aufgrund von: Steigenden Energiepreisen, Für eine Nachhaltige Umwelt sorgen, Für die Zukunft seiner Kinder sorge tragen, selbst anzupacken anstatt zu warten bis die Politik etwas macht,  auf Grund von Förderungen oder wirtschaftlichen Interesse investieren)
*   An Energieoptimierung und schrittweiser Energieunabhängigkeit interessiert
*   Für jedes Budget wollen wir eine passende Lösung haben.  (von Balkonkraftwerk über Komplettanlagen bis Energiefelder))

Motivation:  Energie zu sparen und kosten zu senken ,  auch eine Investition in die Zukunft zu tätigen. Für eine Grüne Zukunft etwas beizutragen:

Ziele: Erträge generieren und Kosten gering halten.  Ein Schritt in ein Nebeneinkommen.

Frustration, Angst: Politik (stagniert) handelt nicht, Angst am Monatsende zu hohe Rechnungen bezahlen zu müssen,  monatlichen Preissteigerungen sind nicht mehr kalkulierbar, Finanzielle sorgen. Kinder in einer schlechten Zukunft zu sehen auf Bezug der Natur oder Arbeitsplätze oder gar Krieg) , Existenzängste,

  

  

Die wichtigsten Kunden fragen:

1.  Wie lang dauerts , was kostet es , Realisierung Zeitfaktor
2.  Förderbar?
3.  Wie läuft es mit der Förderung ab und Projekt Realisierung
4.  Netzausbau was ist mit dem netz
5.    
    
6.  Brauch ich einen Speicher?
7.  Hybridwechselrichter kann der auch Batterie Bespeisen oder nicht. (ist er ein Batterie Wechselrichter)
8.  Blackout Funktion und Notstromfunktion
9.  Speicherfähiger Wechselrichter Hybrid und Inselfähig (Notstrom und Blackout Lösungen)
10.  energiekosten , Investition auch erfolgreich
11.  kann man verkaufen und was ist die Marktpreis Vergütung
12.  anlagen Standort von Speicher und Wechselrichter (Feuerschutz Schalter etc.)
13.  Energie sorge Energieabhängigkeit
14.  Beratung zu den gesamten Thema

  

Frustrationen und Ängste:

*   Keine Rückmeldungen und gute Betreuung beziehungsweise Beratung
*   Angst davor eine fehl Investition zu tätigen
*   stagnierte Umsetzung Zeitfaktor zieht sich in die länge
*   Umsetzungsdauer zu lange
*    Anlage zeitnahe bekommen
*   Zukunft
*   Gesundheit
*   Energiekosten Senken
*   Energieabhängigkeit
*   Zeitnahe Realisierung beinahe unmöglich
*   Stromkosten - Energiekosten
*   Unwissenheit -  auf Bezug Energie Verwaltung
*   Versorgungssicherheit gewährleisten in Energiebezug

  

* * *

Vision Statement:

1.  What . Welches konkrete Ziel will die Marke erreichen:
    1.  Zufriedenheit für Endkunden schaffen, als zuverlässiger Partner fungieren, unkomplizierte Abwicklung ermöglichen, positiv bei Endkunden in Erinnerung bleiben, Großhändler und Energie-contracting Betreiber - Der Ansprechpartner für PV - Lösungen 1- 2 MW Peak im Jahr realisieren (ohne Handel)
    2.  Etabliertes Unternehmen , Marktprägendes Unternehmen,  Engergieanbieter der Marktprägend ist. (Von Energieberatung bis zur Erzeugung)
    3.  Der erste Ansprechpartner im Bereich Energieoptimierung. (Alles was nicht verbraucht wird, muss auch nicht bezogen/hergestellt/bezahlt werden). Mit einem Marktführenden Energiespar Konzept das Land und Kunden beliefern und zum Ziel einer Energie effizienten Gesellschaft und Organisation Umsetzten.
    4.  Für Weißkirchen eine Energiegemeinschaft machen
2.  When - Wann soll das Ziel erreicht werden?
    1.  bis 2025
    2.  2030
    3.  As soon as possible
3.  Where: Wo will die Marke ihr Ziel erreichen: 
    1.  Österreich
    2.  Oberösterreich weit
    3.  International (Ein gutes Konzept etabliert sich durchaus in jeder Organisation) doch vorerst Österreichweit
4.  Zusätze:
    1.  Familiärer Großhandel im Herzen von Oberösterreich
    2.  leer
    3.  Energieoptimierer

  

  

Mission Statement:

  

*   Problem - Was für ein Hauptproblem hat unsere Zielgruppe:
    *   Energiekosten Senken, Energieabhängigkeit, Zeitnahe Realisierung beinahe unmöglich
    *   Stromkosten - Energiekosten, Unwissenheit -  auf Bezug Energie Verwaltung
    *   Versorgungssicherheit gewährleisten in Energiebezug
*   Solution - Welche Lösung bietet die marke
    *   PV- Komplettanlagen, Wirtschaftlichkeitsberechnung, Materialien, Ideenrealisierung, Förderungen beantragen, Beratung
    *   Kompetentes Know how im Sektor Energie, preisgünstige schnelle Lösungen für pv anlagen
    *   Netzwerk, Zeitnahe, Kunde, Partner
*   Success - Was ist der Erfolg eurer Kund:innen als Resultat eures Angebotes:
    *   Nebeneinkommen generieren, kosten Ersparnisse, schnelle Realisierung, umweltschonende grüne Energie
    *   für mehr sorgen Freiheit ,  guten Gefühl für die Umwelt sich eigesetzt zu haben, Unabhängigkeit bis zu einem gewissen grad
    *   Versorgungssicherheit garantiert -Zeitnahe Realisierung - Partnernetzwerk

  

* * *

  

WERTE: (mut, know how , innovation and leadership)

Freundlich, Bewusst, Natürlich, zielstrebig, authentisch, dynamisch, technisch, Handschlag Qualität

Mutig, Bewegend, Cool, Laut, Lebendig, Familiär, Warmherzig, Ehrlich, Maßgebend, etabliert, elitär, fein, selbstbewusst, robust, down to earth, durchdacht, nahbar, praktisch, direkt

  

Rücksichtnahme auf Bezug der Natur und World/Future Changer

  

Freundlich, Maßgebend, Handschlag Qualität, Bewegend, Lebendig, Familiär, Ehrlich, down to earth, nahbar

Laut, Cool, selbstbewusst, durchdacht, direkt|Klar, auffällig, progressiv

  

Maßgebend, Handschlag Qualität, Bewegend,zielstrebig, mutig

Die marken Tonalität:

*   Freundlich anstatt von autoritär
*   progressiv anstatt von konservativ
*   ? klar anstatt von verspielt
*   humorvoll anstatt von ernst
*   auffällig anstatt von dezent
*   ? corporate anstatt von casual
*   ? jung anstatt von etabliert (etabliert das ziel)
*   Masse anstatt eine Nische

  

Marken Werte:

1.  Mit welchen einem Wort würdest du die Marke Beschreiben:
    1.  Kundenzufriedenheit | Bewegend, Vorausschauend
2.  Mit welchen einem Wort würde der Markt die Marke beschreiben:
    1.  Zielorientiert | Neu
3.  Mit welchem einen Wort würden Kund:innen eure marke beschreiben:
    1.  Problemlöser | Zielstrebis, Zuverlässig

  

Markensteuerrad:

1.  Markenwert 1: kompetent | freundlich | ehrlich
2.  Markenwert 2: Ehrlich | dynamisch |
3.  Markenwert 3: down to earth | professionell | vorausschauend , natürlich
4.  Markenkern: familiär | Zielstrebig | MUT

  

* * *

  

  

  

### WHY – WARUM machst du etwas? Was ist dein Sinn?

Das WHY beschreibt deinen sogenannten Purpose, also deinen höheren Sinn, deinen Antrieb oder den Grund deines Handelns. Dies ist der Kern des Golden Circles. Warum hast du dein Unternehmen gegründet? Was möchtest du erreichen oder verändern? Viele vergessen dieses Warum und sehen ihren Sinn nur noch darin, den Laden am Laufen zu halten und Umsätze und Gewinne zu erzielen. Aber die Wenigsten eröffnen eine Physiotherapiepraxis, vorrangig um Geld zu verdienen, sondern um anderen Menschen zu helfen.

  

### HOW – WIE machst du es? Wie gehst du vor und was unterscheidet dich dabei von anderen?

Im zweiten Schritt geht es darum, wie du vorgehst, um dein WHY zu erreichen. Frage dich, welche Methoden oder Strategien du nutzt. Wie kommst du zu deinem Ziel? Metaphorisch stellt das HOW den Weg zu deinem WHY, also deinem Sinn, dar. Im unternehmerischen Kontext umfasst das HOW dein gesamtes [Geschäftsmodell](https://gruenderplattform.de/geschaeftsmodell/was-ist-ein-geschaeftsmodell), in dem du genauestens beschreibst, wie dein Unternehmen funktioniert. Zu dieser Ebene zählt auch dein [USP](https://gruenderplattform.de/unternehmen-gruenden/usp) (Alleinstellungsmerkmal), das dich von der Konkurrenz unterscheidet und deine Kundschaft begeistert.

  

### WHAT – WAS machst du? Was ist das Ergebnis?

Das WHAT ist das Ergebnis der ersten zwei Ebenen und wahrscheinlich am einfachsten zu beschreiben. Hier geht es um konkrete Handlungen, Produkte oder Leistungen. Frage dich, was du anbietest (welche Produkte oder Dienstleistungen). Was macht dein Unternehmen? Diese Ebene ist wesentlich greifbarer, und vielleicht versuchen gerade deswegen viele Unternehmen, sich durch das WHAT vom Wettbewerb abzuheben. Eine Differenzierung über das WHY ist jedoch wesentlich erfolgversprechender, denn sie spricht die Kundschaft auch emotional an. 

  

Wichtig bei der Anwendung des Golden Circles ist also die Reihenfolge, in der du dich mit den drei Ebenen beschäftigst. Im Idealfall startest du mit dem WHY und gehst über das HOW zum WHAT. Aus deinem höheren Sinn, aus dem WARUM ergibt sich, WIE du WAS machst. Dabei sind aber alle Ebenen gleich wichtig. Eine reine Vision, ein höherer Sinn allein ergibt noch kein erfolgreiches Unternehmen. Im Gegenteil, ohne besondere Fähigkeiten und ohne ein einmaliges Angebot bleibt das WHY nur eine leere Hülle. Das HOW und WHAT sorgen dafür, dass deine Vision überhaupt erst glaubhaft und authentisch wird. 

  

  

  

Designe

*   Kernfarben und Layout
    *   Schwarz eher dunkelgrau und weiß
    *   Signalfarbe blau
    *     
        
*   Ral Farben Katalog
*   Schriftart
*   Icon, Logo, Banner
*   Visitenkartendesigne (inkl. Linkedin QR Code)