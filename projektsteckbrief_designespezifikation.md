# Projektsteckbrief-Planung

| Kurzbezeichnung |  “Für mehr Sonnenschein im Leben” |
| ---| --- |
| Titel | PV-Planer Web Anwendung (Netzwerk) |
| Laufzeit: | Start:<br>03.10.2022 | Ende:<br>17.02.2023 |

  

### Versionierung:

  

| <br>Nr.<br> | <br>Datum<br> | <br>Version<br> | <br>Geänderte Kapitel<br> | <br>Art der Änderung<br> | <br>Autor<br> |
| ---| ---| ---| ---| ---| --- |
| <br>1<br> | <br>19.10.2022<br> | <br>1.1<br> | <br>Alle<br> | <br>Erstellung<br> | <br>Dominik Leitgeb<br> |
| <br>2<br> | <br>21.10.2022<br> | <br>1.2<br> | <br>Alle<br> | <br>Content<br> | <br>Dominik Leitgeb<br> |
| <br>3<br> | <br>22.10.2022<br> | <br>1.3<br> | <br>Alle<br> | <br>Bearbeitung<br> | <br>Dominik Leitgeb<br> |
| <br>4<br> | <br>14.11.2022<br> | <br>1.4<br> | <br>Alle<br> | <br>Überarbeitung<br> | <br>Dominik Leitgeb<br> |
| <br>5<br> |  | <br>1.5<br> | <br><br> | <br><br> | <br><br> |
| <br>6<br> |  | <br>1.6<br> | <br><br> | <br><br> | <br><br> |

  

### Projektinhalt und -Ziele

  

| Inhalt & Ziele |
| --- |
| Projektbeschreibung _(~100-150 Wörter)_ |
| <br>Photovoltaik-Anlagenplaner (Webanwendung-Prototyp):<br><br>Das Design Studio findet in Kooperation mit der Firma Solarhype GmbH statt.<br><br>Das Projekt umfasst die Erstellung eines Prototyps für den Anwendungsbereich Photovoltaikanlagen. Dabei handelt es sich um ein Tool, das einerseits für den Kunden alle Fragen beinhaltet, die ein Erstgespräch ersetzen kann, als auch für die Anlagenbauer alle notwendigen Daten sammelt, um eine Projektplanung und ein erstes indikatives Preisangebot ermöglicht.<br><br>Das Hauptaugenmerk dieses Prototyps fokussiert sich auf die Zeitersparnis für den Anlagenbauer und der dabei gewonnenen Sicherheit bei der Datenerfassung. Weiters soll für den Kunden ein intuitiv einfaches und ansprechendes Erlebnis geboten werden. Der Prototyp Umfasst eine "Journey" die als Ergebnis ein Projekt ausgibt bei dem die Anlagenkonzeption des eigenen Projektes und dessen Amortisationszeit im Vordergrund steht.<br>Das Ergebnis dieses Projektes soll dem Kunden das Wissen um eine lohnende Solaranlage anschaulich vermitteln und alle Eckdaten für die Initialisierung des Projektes auf Kundenseite bereitstellen. Das bedeutet, dass dem Kunden alle behördlichen Vorgaben in der Anwendung, aufrufbar, bereitgestellt werden. Diese sollen auf einer einfach zu bedienenden Projekt-Journey über Klicks und Vorauswahlmöglichkeiten basieren. Der erste Prototyp wird in Form eines Mockups erstellt um den Zeitrahmen einzuhalten. Beziehungsweise kann dieser auch gegeben falls in die Website integriert werden. Die zur Verfügung stehenden Ressourcen werden für die Festlegung und Ausarbeitung der notwendigen Rahmenbedingungen für eine weitere Umsetzung der Anwendung konzentriert. Das bedeutet, dass der primäre Fokus auf dem Sammeln von Wissen über die notwendigen Features liegt und der sekundäre Fokus auf der direkten Umsetzung und deren Machbarkeit.<br> |
| Ausgangssituation, Motivation _(~50-100 Wörter)_ |
| <br>Der aktuelle Hype in der PV-Branche hat die Nachfrage explodieren lassen und die Ressourcen vieler PV-Anlagenhersteller an ihre Grenzen gebracht. Da es viele Kunden gibt, die Neulinge auf dem Gebiet der PV sind, werden die ersten Gespräche und die Datenerfassung zunehmend zu einer großen zeitlichen Herausforderung. In diesem Bereich sollte diese Anwendung ihren Platz finden, um die Brücke zwischen Kunde und Anlagenbauer einfach zu schlagen.<br><br>Auch ein Zuwachs an neuen Anlagenbauern lässt sich verzeichnen, diese unterliegen meist noch keiner großen und stabilen Infrastruktur bzw. mangelt es an Ressourcen im Bereich Material oder Arbeitskräfte .<br><br>**Forecast:**<br>Auch die Zahl der neuen Anlagenbauer nimmt zu, von denen die meisten noch nicht über eine große und stabile Infrastruktur verfügen oder denen es an Ressourcen in Form von Material oder Arbeitskräften fehlt.<br>Wenn die Datenerfassung und Datenausgabe eines realistischen Projektes erfolgreich ist, wird in weiterer Folge eine Netzwerkplattform initialisiert. Diese Plattform soll es ermöglichen Projekte und Arbeitsleistung öffentlich zu stellen, damit Kunden, Anlagenbauer, Installateure, Elektriker, Materialhändler und Immobilienbesitzer deren Ressourcen und Projekte öffentlich machen zu können damit diese ehestmöglich Umgesetzt beziehungsweise ausgeschöpft werden können.<br> |
| Projekt Ziele (mit Teilzielen in Aufzählungsform) _(~100 Wörter)_ |
| Ziel ist es, einen Prototyp für eine Web-Anwendung im Bereich der PV-Anlagen zu entwickeln. Dieser soll als Grundlage für die reale Projektierung genutzt werden können. Zu diesem Zweck wird eine detaillierte Analyse im Bereich der Datenerfassung für PV-Anlagen durchgeführt. Die aus dieser Analyse gewonnenen Ergebnisse können über die Anlageninitialisierung informieren, können potentielle Kunden mit wertvollen Informationen versorgen, können lohnende PV-Anlagen anzeigen und sollen Aufschluss darüber geben, welche Features die Webanwendung in Zukunft bereichern können.<br><br>Die Web Anwendung für die der Prototyp erstellt wird, sollte die folgenden Funktionalitäten haben:<br><br><br><br><br><br><br><br><br><br>Visionäre optionale Ziele:<br><br><br><br><br><br><br><br><br><br><br><br><br> |
| Projektnutzen & Zielgruppenbeschreibung _(~100 Wörter)_ |
| <br>**Wer soll von dieser Anwendung profitieren? Konkret, wen wollen wir mit dieser Anwendung ansprechen?**<br><br><br>**Zwei Zielgruppen:**<br>**Primäre Zielgruppe ---- Kunden:**<br><br>Eine Privatperson, die eine Photovoltaikanlage für ihr Einfamilienhaus installieren möchte und keine Vorkenntnisse in diesem Bereich hat. All die Fragen, die diese Person nun hat, wollen geklärt werden. Wie starte ich das Projekt, wer kann das Projekt für mich realisieren und wann ist dieses Projekt für einen rentabel.<br><br><br>**Sekundäre Zielgruppe ---- Unternehmen:**<br><br>Unternehmen, die einen akuten Fachkräftemangel haben, können Zeit sparen und sich um die wichtigen Aufgaben kümmern. Diese Unternehmen können sich nun auch untereinander vernetzen, um Materialengpässe auszugleichen und anderen "Projekten" mit Fachkräften auszuhelfen.<br><br><br>**Nutzen für Unternehmen/Anlagenbauer:**<br><br><br><br><br><br>**Nutzen für den Kunden:**<br><br><br><br><br><br><br>**(Optional) Nutzen für Immobilienbesitzer und Ressourcenbesitzer:**<br><br><br><br><br><br><br><br>**Welchen Mehrwert bringt unser Produkt/Dienstleistung? Was macht es besonders, auch im Vergleich zu anderen Produkten/Dienstleistungen?**<br><br><br>Können wir den Mehrwert unserer Anwendung schnell und klar definieren, heben wir uns schneller mit exklusiven Anwendungen ab:<br><br><br><br><br> |
| Abgrenzungen: Nicht-Ziele |
| <br><br><br> |

  

### Ressourcen & Budget

  

| Projektteam |
| --- |
| Projektleiter |
|  Dominik Leitgeb |
| Projektteam |
| <br>Partner: Solarhype GmbH<br> |

  

| Ressourcen |
| --- |
| Personalaufwand |
| Projektumfang Design Studio:<br>Das Projekt startet ab dem Zeitpunkt, der Fertigstellung des Pflichtenhefts. Bis zu diesem Zeitpunkt werden ca. 100 Stunden geleistet.<br><br><br><br>Somit bleibt für die Umsetzung des Projektes ein Zeitaufwand von ca. 200 Stunden.<br><br> |
| Projektbudget |
|  12 ECTS = 300 Stunden |
| Sonstige Aufwände |
| Drittanbieter-Software: Budget ….€<br><br> |

  

Projektgliederung, Termine und Risikoanalyse
--------------------------------------------

  

### Arbeitspaketübersicht:

| AP | Name | Laufzeit |
| ---| ---| --- |
| 1 | AP1 Konzeptionierung & Planung | 10.2022 – 01.2023 |
| 2 | AP2 Zielgruppendefinition, Nutzen & USP | 10.2022 – 01.2023 |
| 3 | AP3 Recherchearbeit für Anwendungsgebiete | 10.2022 – 01.2023 |
| 4 | AP4 Konkurrenzanalyse & Risikomanagement | 10.2022 – 01.2023 |
| 5 | AP5 Rechtliche Rahmenbedingungen | 10.2022 – 01.2023 |
| 6 | AP6 Prototyp | 10.2022 – 01.2023 |
| 7 | AP7 Fördermöglichkeiten & Anträge | 10.2022 – 01.2023 |

  

### Detaillierter Arbeitsplan

| AP1 | Konzeptionierung & Planung | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson |  Projektteam |
| Dominik Leitgeb |  |
| Ziel |
| <br>Das Hauptziel ist die Aufrechterhaltung des Kommunikationsflusses. Dies geschieht in Form von einem wöchentlichen Meeting mit der Firma Solarhype GmbH. Weitere Ziele sind eine gute Strukturierung des Arbeitspensums, um Zeit zu sparen und eine reibungslose Projektdurchführung zu gewährleisten. Durch gezielte regelmäßige Überprüfungen des Zeitplans und seiner Umsetzung kann man auf mögliche Hindernisse und Herausforderungen schließen und so darauf reagieren.<br><br>Das Projekt wird in 3 Phasen unterteilt, um die Übersicht und Effizienz zu erhöhen.<br><br><br><br><br> |
| Inhalt: Aufgaben/Tasks |
| <br>**Projektinitialisierung & Konzeptionierung (45h)**<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>**Projektdefinition und Planung (45h)**<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>**Projekt Performance & Kontrolle (15h)**<br><br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Als erwartetes Ergebnis soll in ClickUp ein Leitfaden und Projektplan erstellt werden, der leittragend für die korrekte und geplante Umsetzung ist und genügend Flexibilität für Kontrolle und ungeplante Änderungen bietet, um diese problemlos einfügen zu können. Die Aufgaben werden editierbar sein und verschiedene visuelle Funktionen von ClickUp werden helfen, einen übersichtlichen Zeitplan zu erstellen (Gantt und Timeline). ClickUp soll helfen, Planung, Durchführung und Kontrolle im Überblick zu behalten.<br> |
| Meilensteine & Deliverables |
| M 1.1: Pflichtenheft finish | 05.11.2022<br>M 1.2: Projekt Performance & Kontrolle | 14.12.2022<br><br>D 1.1: ClickUp Abgabe für Design Studio | 26.01.2023<br> |

  

| AP 2 | Zielgruppendefinition, Nutzen & USP | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Das Ziel dieses Arbeitspaketes ist es, die Lücke zwischen dem Nutzen als Anbieter und dem Nutzen als Kunde zu schließen. Es werden also 2 Zielgruppen gleichermaßen angesprochen.<br><br>Nutzen für Unternehmen:<br><br><br><br><br><br>Nutzen für den Kunden:<br><br><br><br><br><br><br>Nutzen für Immobilienbesitzer und Ressourcenbesitzer:<br><br><br><br><br> |
| Inhalt: Aufgaben/Tasks |
|  |
| <br><br><br><br><br><br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Das Ergebnis dieses AP sollte sein:<br><br><br><br><br><br> |
| Meilensteine & Deliverables |
| M 2.1: USP und Wertefindung | 11.11.2022<br>D 2.1: |

  

| AP 3 | AP3 Recherchearbeit für Web-Anwendungen | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Ziel ist es, dem Nutzer die für ihn passende Photovoltaikanlage zu vermitteln. Beim erstmaligen Betreten der Anwendung bzw. über einen Button zugreifbar sind Fragen zu beantworten, aufgrund deren Auswertung der Nutzer einer bestimmten Anlagenkategorie zugeordnet werden kann und als Ergebnis ein Dokument (Stammdatenblatt) für den Kunden des jeweiligen Projektes hinterlegt wird, das für die Projektierung des Projektes notwendig ist. Der Benutzer erhält automatisch ein Projekt der jeweiligen Kategorie. Das Projekt kann vom Benutzer bearbeitet werden, um das Projekt und die zugehörigen Werte auf speziell angepasste Werte zu ändern.<br><br>Forecahst (Optional):<br>Die von der Anwendung generierten Vorschläge für Projektkosten (Richtpreise), Stromkosten, Anschlussgebühren etc. basieren auf einer von der Anwendung bereitgestellten Datenbank, deren Werte auf Basis von Regionen und Durchschnittswerten ausgewählt werden.<br> |
| Inhalt: Aufgaben/Tasks |
| <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Für die Datenerfassung:<br>Alle notwendigen Daten für das anstehende Projekt werden ausgearbeitet und in einem Dokument abgelegt. Ziel ist es, die ersten Gespräche auf ein Minimum zu reduzieren und sofort mit der Projektplanung beginnen zu können. Um sofortige Preisangebote über die Anwendung anbieten zu können, die einen genaueren Hinweis auf die Amortisation des Projekts geben.<br><br>Für den Benutzer:<br>Einfache, für jedermann verständlich formulierte Fragen sollen den Entscheidungsprozess und die Aufklärung zum Thema Photovoltaikanlagen schnell klären. Projektvorschläge, die individuell bearbeitet und angepasst werden können, sollen den Entscheidungsprozess vereinfachen und beschleunigen.<br> |
| Meilensteine & Deliverables |
| M 3.1: Research Ende | 09.11.2022<br><br>D 3.1:<br> |

  

| AP 4 | Konkurrenzanalyse & Risikomanagement | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Ziel ist es, bestehende Lösungen auf dem Markt abzubilden. Mögliche Konkurrenten aufzuzeigen und unseren Mehrwert im Vergleich zu den anderen Anbietern deutlich darzustellen. Im Folgenden ist es auch wichtig, Lösungen von Drittsoftware abzubilden, um mögliche Partner in diesem Bereich integrieren zu können.<br>Ebenso sollen in diesem Arbeitspaket mögliche Risiken herausgearbeitet und identifiziert werden.<br><br> |
| Inhalt: Aufgaben/Tasks |
| <br><br><br><br><br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Es wird erwartet, dass die Ergebnisse aus diesem Bereich Erkenntnisse für die Marktpositionierung liefern und Verbesserungen in Bezug auf den Prototyp ableiten lassen.<br><br>Es werden Informationen gesammelt über:<br><br><br><br>Darüber hinaus wird eine Risikoanalyse durchgeführt und im Projektsteckbrief festgehalten.<br> |
| Meilensteine & Deliverables |
| <br>M 4.1: Konkurrenz ausführlich durchleuchtet | 30.11.2022<br><br>D 5.1:<br> |

  

| AP 5 | Rechtliche Rahmenbedingungen | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Ziel ist es, die rechtlichen Säulen für und bei der Erstellung einer Web-Anwendung aufzuzeigen, um alle notwendigen Stellen mit der Umsetzung beauftragen zu können.<br><br>Vorrangiges Ziel ist es, den erforderlichen Rechtsrahmen zu schaffen.<br><br> |
| Inhalt: Aufgaben/Tasks |
| <br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Das erwartete Ergebnis ist eine Liste der erforderlichen rechtlichen Rahmenbedingungen.<br> |
| Meilensteine & Deliverables |
| <br>M 5.1: Liste der benötigten Rechtsrahmen fertiggestellt | 15.01.2023<br><br><br>D 5.1:<br> |

  

| AP 6 | Prototyp | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Das Ziel ist es, einen Prototypen zu erstellen. Dies sollte eine zugängliche Lösung sein, bei der man visuell durch einen vordefinierten Entscheidungsbaum geführt wird und die notwendigen Ausgaben generiert, die diese Web-Anwendung zu erfüllen hat.<br> |
| Inhalt: Aufgaben/Tasks |
| <br><br><br><br><br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Als Ergebnis wird eine erste Nutzererfahrung mit dieser Web-Anwendung erwartet. Die Funktionen der Web-Anwendung werden nur teilweise eingebettet sein und weitere Features werden erst bei der weiteren Umsetzung des Projekts berücksichtigt.<br><br>Vor allem die Implementierung des Entscheidungsbaums mit dem dafür vorgesehenen Fragenkonzept wird im Prototypen zu testen sein.<br><br>Darüber hinaus sollen die Rahmenbedingungen und Features der Anwendung geklärt und als Konzept ausgearbeitet werden.<br><br> |
| Meilensteine & Deliverables |
| M 6.1: Entscheidungsbaum fertiggestellt | 23.12.2022<br>M 6.2: Prototyp fertiggestellt | 23.01.2023<br><br>D 6.1: Prototyp<br> |

  

| AP 7 | Fördermöglichkeiten & Anträge | 10.2022 – 01.2023 |
| ---| ---| --- |
| Ansprechperson | Projektteam |
| Dominik Leitgeb | Team |
| Ziel |
| <br>Ziel ist es, die möglichen Förderungen im digitalen Bereich herauszufinden und aufzulisten. Darüber hinaus sind Start-up-Förderung und Projektförderung von Interesse und ebenfalls Teil der Recherche.<br> |
| Inhalt: Aufgaben/Tasks |
| <br><br><br><br><br><br> |
| Erwartete Ergebnisse |
| <br>Als erwartetes Ergebnis sollte ein Konzept definiert werden, das die Fördermöglichkeiten und deren zeitliche Nutzung aufzeigt, um dies entsprechend dieser Struktur umsetzen zu können.<br><br> |
| Meilensteine & Deliverables |
| M 7.1: Förderkonzept fertiggestellt<br><br>D 7.1: Es sind keine spezifischen Leistungen für das Designstudio erforderlich. |

  

  

  

### Meilensteinplan

  

| Name | Fertigstellung |
| ---| --- |
| M 1.1 | Pflichtenheft finish | 05.11.2022 |
| M 1.2 | Projekt Performance & Kontrolle | 14.12.2022 |
| D 1.1 | ClickUp Abgabe für Design Studio | 26.01.2023 |
| M 2.1 | USP und Wertefindung | 11.11.2022 |
| M 3.1 | Research Ende | 09.11.2022 |
| M 4.1 | Konkurrenz ausführlich durchleuchtet | 30.11.2022 |
| M 5.1 | Liste der benötigten Rechtsrahmen fertiggestellt | 15.01.2023 |
| M 6.1 | Entscheidungsbaum fertiggestellt | 23.12.2022 |
| M 6.2 | Prototyp fertiggestellt | 23.01.2023 |
| M 7.1 | Förderkonzept fertiggestellt |  |

  

### Gantt Chart

  

In ClickUp abrufbar

  

  

### Risikoanalyse

  

_(\[__G__\]ering, \[ M \]ittel,\[__H__\]och\], \[B\]ereinigt)_

  

| Nr.: | Risiko | Bewältigungsstrategie | Art | Aktualisierung |
| ---| ---| ---| ---| --- |
| 1 | Ausfall von Arbeitskapazitäten<br> | Aufgaben der Person werden übernommen → erhöhter Arbeitsaufwand für das restliche Team | <br>H<br> | 17.10.2022 |
| 2 | Schlechtes Zeitmanagement, kein gut ausgearbeiteter Zeitplan<br><br> | erhöhter Arbeitspensum→ Zeit aufholen | <br>N<br> |  |
| 3 | unterschätzter Arbeitsaufwand einzelner AP´s<br> | Verantwortlicher muss Arbeitspensum erhöhen oder Team kompensiert, Inhalte runterbrechen auf das Notwendigste | <br>H<br> |  |
| 4 | Arbeitspakete wurden nicht detailliert genug festgelegt<br> | Ausgangssituation + Details neu und detailliert festlegen | <br>G<br> |  |
| 5 | unausgesprochene Konflikte sabotieren die Motivation innerhalb des Teams<br> | Konfliktbewältigung | <br>M<br> |  |
| 6 | unklare Aufgabenverteilung | Aufgaben werden klar und deutlich einem Teammitglied zugeteilt und der Fortschritt muss dokumentiert werden | <br>G<br> |  |
| 7 | Unterschätzung von Risiken | Systematische Risikoanalyse | <br>M<br> |  |
| 8 | Teil von den AP´s ist noch nicht fertig | Fokus auf wichtigere Bereiche legen, wenn trotzdem möglich Zeitmanagement anders einteilen, auf andere Bewältigungsstrategie zurückgreifen | <br>M<br> |  |
| 12 | Ausfall von Software | auf andere Software zurückgreifen, und Backup regelmäßig abspeichern | <br>G<br> |  |

  

| Freigabe |
| --- |
| Freigabe: | <br>Datum:<br> | 07.11.2022 |
| <br><br><br> |  |
| Unterschrift Auftraggeber | Unterschrift Projektleiter |