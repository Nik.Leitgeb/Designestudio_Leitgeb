# Endbericht

Inhaltsverzeichnis:
===================

  

1.  Einleitung
    1.  Projektziele
    2.  Projekthintergrund
2.  Anwendungsbeschreibung
    1.  Funktionsweise
    2.  Technologien
    3.  Designspezifikationen
3.  Ergebnisse
    1.  Zeitersparnis
    2.  Ressourcenersparnis
    3.  Kosteneinsparung
    4.  Erfüllung der Designspezifikationen
4.  Fazit
    1.  Zusammenfassung
5.  Anhänge
    1.  Screenshots der Anwendung
    2.  Quellcode der Anwendung
    3.  Testberichte
    4.  Clickup Plattform

* * *

  

1.  Einleitung:

Der vorliegende Bericht befasst sich mit dem Ergebnis des Projekts, das die Entwicklung einer Webanwendung zum Ziel hatte. Das Projekt begann mit der Definition der Anforderungen und der Erstellung eines Entwurfs der Benutzeroberfläche. Dabei wurden im sinne von "Designe-Thinking" mehrere gesprächsschleifen eingeleitet. Dabei sind wir in gemeinsamer Arbeit zu dem Entschluss gekommen eine Corporate Identity durchzuführen um ein verbessertes Gesamtergebnis zu erzielen. Zu diesem Zweck wurden eine Reihe von Meeting mit Mitarbeitern durchgeführt, um Marke, Mission, Vision und Ziele neu zu durchleuchten. Eine Marktanalyse, sowie eine Recherchearbeit zu den benötigten Daten wurde durchgeführt, um zu bestimmen, welche Funktionen und Schnittstellen für die Webanwendung benötigt wurden. (Verweis: [Recherche-Stammdaten.md](http://Recherche-Stammdaten.md) oderPrivate ([https://app.clickup.com/43259044/v/dc/198554-7764](https://app.clickup.com/43259044/v/dc/198554-7764))) Nach der Erstellung der Benutzeroberfläche wurden die technischen Anforderungen erfüllt, indem die verschiedenen Komponenten der Webanwendung entwickelt und getestet wurden. Dazu gehörten eine Reihe von Web- und Datenbankservern, die für die Bereitstellung und Verwaltung der Anwendung verantwortlich waren. Nachdem die technischen Anforderungen erfüllt waren, wurde die Webanwendung erfolgreich gesteuert und überwacht. Die Firma SolarHype hat eine Reihe von Tests durchgeführt, um sicherzustellen, dass die Webanwendung den Anforderungen entsprach und die gewünschten Ergebnisse erzielte. Das Projekt hat sich als sehr erfolgreich erwiesen und die Webanwendung wird nun erfolgreich von den Kunden der Firma Solarhype genutzt. Einzelheiten über die Ergebnisse des Projekts und der Webanwendung sind im Anhang zu diesem Bericht zu finden. Aufgrund der positiven Ergebnisse des Projekts und der Webanwendung ist es möglich, weitere Verbesserungen vorzunehmen, um die Benutzererfahrung zu verbessern und die Nutzung der Webanwendung noch einfacher und effizienter zu machen. Zusammenfassend kann gesagt werden, dass das Projekt erfolgreich abgeschlossen wurde und die Webanwendung jetzt erfolgreich im Einsatz ist.
1.  Projektziele:
    1.  Das Ziel dieser Anwendung war es, Arbeitsschritte, Zeit und Ressourcen zu sparen und die Designspezifikationen zu erfüllen. Wir können sagen, dass das Ziel erreicht wurde. Die Anwendung hat es ermöglicht, die Arbeitsschritte zu vereinfachen und die Ressourcen zu sparen. Es hat es auch ermöglicht, jedes Designkriterium zu erfüllen. Es wurden verschiedene Funktionen implementiert, um die Anforderungen des Designs zu erfüllen. Die Anwendung wurde erfolgreich getestet und es wurde keine signifikante Fehler festgestellt. Alle Funktionen funktionierten, wie es erwartet wurde. Insgesamt können wir sagen, dass die Anwendung erfolgreich entwickelt wurde. Es hat erfolgreich die Anforderungen erfüllt und es wurden Arbeitsschritte und Ressourcen gespart.
2.  Projekthintergrund:

Bei der Firma SolarHype haben mehrere Faktoren dazu geführt, dass ein großer Bedarf an Prozessverbesserungen bestand. Darüber hinaus wurde zur Lösung des Problems eine neue Sichtweise auf die Materie gewünscht. Die folgenden Punkte waren die Leitgedanken:
1.  Zeit: Zeitverlust aufgrund eines Prozesses, der viele Kommunikationsschritte erforderte, bevor ein Angebot versandt wurde.
2.  Kunden-Flut: Die Zunahme der Anfragen um fast 200 % stellte eine Herausforderung für die rechtzeitige Bearbeitung dar.
3.  Usability: Es waren keine einfachen und einheitlichen Formulare vorhanden, diese haben  bei den Kunden die Datenerfassung erschwert.
4.  Vertrauen: Vor allem aber brauchen Kunden vertrauen in Ihren Partner. Es geht immerhin um große Investitionen.

3.  Anwendungsbeschreibung:

Corporate Identity:

Die Corporate Identity (CI) beinhaltet die visuelle Identität eines Unternehmens. Dazu gehören z.B. Logo, Farbschema, Schriftarten, Bilder, Slogans und andere Elemente, die ein einheitliches visuelles Erscheinungsbild erzeugen. Ziel ist es, ein einheitliches Erscheinungsbild und eine nachhaltige Markenidentität zu schaffen.

  

Designe:

Design bezieht sich auf den Prozess der Entwicklung eines visuellen Erscheinungsbildes für ein Unternehmen. Dazu gehören die Entwicklung eines Logos, eines Farbschemas, einer Schriftart, Bilder, Slogans und anderer Elemente. Design ist ein wichtiger Bestandteil der CI, da es die visuelle Identität eines Unternehmens definiert und kommuniziert.
*   Corporate Identity und Designe (Marke und Meeting Ablage)
    
    Private ([https://app.clickup.com/t/2xqhjz4](https://app.clickup.com/t/2xqhjz4)) oder im Ordner ➝ 09\_Corporate-Identity-Solarhype/Marke
    
    Private ([https://app.clickup.com/t/30rzrwt](https://app.clickup.com/t/30rzrwt)) oder im Ordner ➝ 09\_Corporate-Identity-Solarhype\\Einzelmeeting
    
    Verweis auf erstellte Texte: solarhype\_webseiten\_content\_.mdPrivate ([https://app.clickup.com/43259044/v/dc/198554-7844](https://app.clickup.com/43259044/v/dc/198554-7844))
    

  

Formular für Datenerhebung einer Photovoltaik Komplettanlage:

Das Formular für die Datenerhebung einer Photovoltaik Komplettanlage ist ein wichtiger Bestandteil der CI. Es ermöglicht es dem Unternehmen, relevante Informationen über den Standort, den Energiebedarf und die technischen Anforderungen zu sammeln, um eine effiziente und kostengünstige Installation zu ermöglichen.
*   Formular für Datenerhebung einer Photovoltaik Komplettanlage ([https://solarhype.at/angebot-einholen/](https://solarhype.at/angebot-einholen/))

  
1.  Funktionsweise des Formulars:

Das Formular für die Datenerhebung einer Photovoltaik-Komplettanlage ist ein Tool zur Erfassung der Informationen, die für die Planung und Errichtung einer Photovoltaikanlage erforderlich sind. Mit diesem Formular können alle relevanten Daten zur Anlage erfasst werden. Darunter fallen Informationen zur Anlagegröße, zur Art des Solarmoduls, zu den Anschlusskomponenten sowie zur Anlagenleistung. Darüber hinaus wird auch nach Informationen zum Eigentümer, zur Anlagenadresse, zu den technischen Kontaktinformationen und der Ansprechperson für die Wartung der Anlage gefragt. Außerdem werden Angaben zum technischen Status der Anlage, zur aktuellen Energieproduktion und den aktuellen Verbrauchswerten erfasst. Mit diesem Formular können alle relevanten Daten einer Photovoltaikanlage erfasst und überprüft werden.

3.  Technologien:

WordPress Formulare verwenden eine Kombination aus HTML, CSS, JavaScript und PHP-Programmierung. HTML ermöglicht dem Benutzer, das Formular zu erstellen, während CSS und JavaScript das Aussehen und das Verhalten des Formulars definieren. PHP wird verwendet, um die eingegebenen Daten zu verarbeiten und zu speichern.

5.  Designspezifikationen:

Designspezifikationen basierend auf "design thinking" sind ein Ansatz, der es Nutzern ermöglicht, das bestmögliche Ergebnis zu erzielen. Dieser Ansatz zielt darauf ab, die bestmögliche Lösung zu finden, indem Nutzer zur Entwicklung beitragen. Dazu gehört die Identifizierung der Anforderungen und Ziele, die für ein erfolgreiches Design erforderlich sind, sowie die Überlegung, wie diese Anforderungen umgesetzt werden können. Um dauerhaft erfolgreiche Designspezifikationen zu erzielen, müssen die Nutzer aktiv an der Entwicklung beteiligt werden. Ein Prozess, der Iterationen und Testphasen beinhaltet, muss eingerichtet werden, um sicherzustellen, dass die Anforderungen der Nutzer erfüllt werden. Auch wenn sich die Anforderungen ändern, muss das Design angepasst werden, um die bestmöglichen Ergebnisse zu erzielen. Darüber hinaus müssen ausreichende Ressourcen bereitgestellt werden, um die Entwicklung des Designs schnell und effizient durchführen zu können. Dazu gehören das richtige Werkzeug, die richtige Personalkapazität und die notwendigen finanziellen Mittel. Letztendlich müssen alle Beteiligten an einem Strang ziehen, um sicherzustellen, dass das Design den Anforderungen des Nutzers entspricht und dass es dauerhaft angepasst werden kann, um die bestmöglichen Ergebnisse zu erzielen.

Hier möchte ich auf den Projektsteckbrief verweisen: projektsteckbrief\_designespezifikation.md oder Private ([https://app.clickup.com/43259044/v/dc/198554-7384](https://app.clickup.com/43259044/v/dc/198554-7384))

Newsroom:

Der Newsroom ist ein wichtiger Bestandteil der CI. Er ermöglicht es dem Unternehmen, seine Nachrichten, Produkte und Dienstleistungen zu veröffentlichen und Kunden und Interessenten über das Unternehmen und seine Aktivitäten zu informieren. Dazu gehören auch Pressemitteilungen, Blog-Beiträge, Videos und Social-Media-Aktivitäten.
*   *   *   Newsroom ([https://solarhype.at/newsroom/](https://solarhype.at/newsroom/))

  

  

  

1.  Ergebnisse
    1.  Zeitersparnis:
    
    Ein Webformular ist ein Werkzeug, mit dem Daten auf einfache und effiziente Weise erfasst werden können. Mit Hilfe eines Webformulars können Formulare schnell und einfach erstellt werden, was wiederum die Zeitersparnis für die Erfassung der Daten erhöht. Diese Zeitersparnis ist vor allem für Unternehmen, die eine große Anzahl von Daten erfassen müssen, von großem Vorteil. Außerdem können Webformulare auch so konfiguriert werden, dass sie alle rechtlichen Rahmenbedingungen erfüllen. Dies ist besonders wichtig, wenn die Daten, die erfasst werden, personenbezogene Daten enthalten. Durch die Einhaltung der rechtlichen Rahmenbedingungen wird sichergestellt, dass die Daten sicher und vertraulich behandelt werden. Dies ist wiederum eine weitere Methode, um Zeit zu sparen und den Datenschutz zu gewährleisten.
    
    3.  Ressourcenersparnis:
    
    Darüber hinaus kann die Erfassung von Daten mit einem Webformular auch dazu beitragen, die Arbeit der Mitarbeiter zu erleichtern, indem sie nicht mehr die Daten manuell auswerten müssen. Stattdessen können sie einfach auf die vorformatierten Formulare zugreifen, um die gewünschten Informationen schnell zu erhalten. Dies spart nicht nur Zeit, sondern auch Ressourcen, da weniger Arbeit erforderlich ist, um die Daten auszuwerten.
    
    5.  Kosteneinsparung:
    
    Ein Webformular, das Daten erfasst, kann zu erheblichen Kosteneinsparungen führen. Durch das Erfassen von Daten über ein Webformular wird die Notwendigkeit, manuelle Eingaben durchführen zu müssen, eliminiert, was zu einer Zeitersparnis und einer Einsparung von Arbeitskräften führt. Des Weiteren können Daten automatisch in ein System eingegeben werden, wodurch die Notwendigkeit entfällt, sie manuell einzutippen. Dies kann zu einer weiteren Einsparung von Arbeitskräften und Kosten führen. Darüber hinaus kann ein Webformular dabei helfen, Fehler bei der Dateneingabe zu minimieren, was zu einer weiteren Kostenersparnis führt.
    
    7.  Erfüllung der Designspezifikationen:
    
    Die Designspezifikationen für das Projekt wurden erfolgreich umgesetzt. Zunächst wurde ein Design-Framework entwickelt (in Form des Projektsteckbriefs), das eine einfache und skalierbare Struktur für die Entwicklung des Projekts bietet. Das Framework enthält eine allgemeine Beschreibung und beinhaltet die Arbeitspakete die Umgesetzt werden wollen. Als nächstes wurde ein Design-System erstellt, das die Benutzeroberfläche des Projekts definiert. Es enthält ein Farbschema, Schriftarten, Benutzeroberflächen, Komponenten und Interaktionen. Mit dem Design-System konnte die Benutzeroberfläche konsistent gestaltet werden, wodurch eine einheitliche und anpassungsfähige Benutzererfahrung gewährleistet wird. Schließlich wurde ein Design-Prozess entwickelt, der es den Entwicklern ermöglicht, das Projekt schnell und effizient zu entwickeln. Der Prozess umfasst die Definition der Anforderungen, die Erstellung eines Designs, die Erstellung eines Prototyps, die Implementierung des Designs und Tests. Mit dem Design-Prozess konnten alle Änderungen an das Design schnell und effizient vorgenommen werden und das Projekt konnte so schnell wie möglich veröffentlicht werden. (Verweis auf die Projektplattform in Clickup oder im Anhang des Endberichts werden Bilder angeführt)
    
2.  Fazit
    1.  Zusammenfassung
    
    Bei der Erstellung eines Webformulars zur Erfassung von Daten für Photovoltaik-Komplettanlagen müssen einige Dinge beachtet werden, um eine effiziente Erfassung und Verarbeitung der Daten zu gewährleisten. Zunächst müssen alle relevanten Datenfelder auf dem Formular angegeben werden. Dazu zählen etwa die Art der Photovoltaikanlage, die Anzahl der Module, die Größe des Speichers und die Anzahl der Anschlüsse. Weiterhin müssen die Anforderungen an die Benutzerfreundlichkeit des Formulars eingehalten werden. Es sollte intuitiv zu bedienen sein und eine einfache Navigation bieten. Außerdem ist es wichtig, dass das Formular validiert werden kann, um sicherzustellen, dass alle erforderlichen Informationen korrekt eingegeben werden. Schließlich müssen die Daten in einem geeigneten Format gespeichert werden, z.B. in einer Datenbank, damit sie leicht zugänglich und verarbeitbar sind.
    
    3.  Empfehlungen
    
    Um ein effektives web Formular und einen Blog für die Datenerfassung und das Marketing zu erstellen, empfehlen wir dem Unternehmen, eine Softwarelösung wie WordPress zu verwenden. Diese Plattformen bieten alle Funktionen und Tools, die für ein erfolgreiches Marketing und die Datenerfassung erforderlich sind. Bei der Wahl der Softwarelösung sollte das Unternehmen sicherstellen, dass es eine benutzerfreundliche Oberfläche hat, die einfach zu bedienen ist. Außerdem sollte die Software eine intuitive Benutzeroberfläche bieten, damit das Unternehmen seine Daten schnell und effizient verwalten und verarbeiten kann. Darüber hinaus ist es wichtig, dass die Software ein integriertes System für die Verfolgung von Reaktionen, Klicks und Interaktionen bietet, damit das Unternehmen die Effektivität seiner Kampagnen messen kann. Schließlich ist es wichtig, dass das Unternehmen regelmäßige Updates für seine Software erhält. Dadurch können neue Funktionen und Tools hinzugefügt werden, um das Marketing und die Datenerfassung zu verbessern. Insgesamt ist es wichtig, dass das Unternehmen eine Softwarelösung wählt, die den spezifischen Anforderungen seines Unternehmens entspricht.
    
3.  Anhänge
    1.  Screenshots der Anwendung (Verweis auch auf: [https://solarhype.at/](https://solarhype.at/)):
    
    ![](https://t43259044.p.clickup-attachments.com/t43259044/aad1c766-1b8d-4f12-a2cd-6aae4d442b9f/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/e595f843-00eb-4c9b-981b-52c86dfac5a2/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/06b36dd9-6c3f-473d-8fad-a4ecbb22cb53/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/ef631265-cf17-4866-9d99-55aff0e35c3a/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/2585ead5-333d-492d-b869-b8ea7fad6c45/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/161ca205-6d0d-4f03-a8b5-8ddb6126ea93/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/67aca206-23ec-4d7e-8f98-43fa4917f7dc/image.png)![](https://t43259044.p.clickup-attachments.com/t43259044/c2a59c00-9e55-4df4-bc8d-7613cc462668/image.png)
    
    3.  Quellcode der Anwendung
    
    [https://github.com/n-d-l/Designstudio](https://github.com/n-d-l/Designstudio)
    
    5.  Testbericht:
    
    Das Webformular wurde getestet, um zu sehen, wie es sich beim Sammeln von Daten für Photovoltaik-Komplettanlagen verhält. Es ist ein einfaches und intuitives Formular, mit dem Benutzer problemlos ihre Informationen eingeben können. Die Benutzeroberfläche ist sehr benutzerfreundlich und gut strukturiert. Es ist einfach, durch das Menü zu navigieren, und es gibt auch klar definierte Felder, die den Benutzern den Eingabeprozess erleichtern. Es gibt auch spezifische Felder, in die der Benutzer Informationen eingeben muss, die zur Berechnung der Photovoltaik-Komplettanlage erforderlich sind. Das Formular ist auch sehr schnell und reagiert schnell auf Eingaben. Es gibt keine Verzögerungen oder Verzögerungen beim Ausfüllen des Formulars. Außerdem ist es sicher, da es die Daten des Benutzers verschlüsselt. Insgesamt ist das Webformular eine sehr gute Lösung für das Sammeln von Daten für Photovoltaik-Komplettanlagen. Es ist benutzerfreundlich, einfach zu bedienen, schnell und sicher. Es ist daher eine großartige Option für Benutzer, die Daten für Photovoltaik-Komplettanlagen sammeln müssen. Folgend einige Änderungen die nach dem Release des Prototypen ausgeführt wurden:
    1.  Problem der Schriftartformatierung behoben, es wurde fälschlicherweise immer der Anfangsbuchstabe großgeschrieben.
    2.  Strom Speicherlösung bin ich mir nicht sicher... Speichergröße die Erklärung weglassen. wirkt wie wenn man des so fix nehmen müsste
    3.  ca. 4000 kWh/Jahr 2 kWp Anlagenleistung Grau hinterlegt 10kWp
    4.  Warum gibt's hier eine Checkbox?
    5.  Zählpunktnummer ist eine - würd i weglassen. Oder gleich eine AT33.....Nummer als Vorlage?
    6.  10kWp Modulleistung oder Photovoltaikanlage
    7.  Die Farbe Schwarz war zu erdrückend, nun wurde sie etwas aufgehellt:
    
    ![](https://t43259044.p.clickup-attachments.com/t43259044/8cde6ca7-caed-4c0d-b5f9-abf23d8b8fe5/image.png)
    
    7.  Clickup Plattform

  

Unten sehen Sie das Gantt-Diagramm, das eine einfache Überwachung des Projekts ermöglichte:

![](https://t43259044.p.clickup-attachments.com/t43259044/50db2482-4f58-4975-9ce3-ee1476c51b97/image.png)

Als weiteres Tool für das Monitoring war die Timeline:

![](https://t43259044.p.clickup-attachments.com/t43259044/1d00aa6c-3ccb-471d-8fb8-9935b3cc627a/image.png)

  

In der Tabelle können Sie alle Aufgaben nach der jeweiligen Kategorie filtern und sich so einen schnellen Überblick über das Projekt verschaffen:

![](https://t43259044.p.clickup-attachments.com/t43259044/558df84d-4e29-4ef8-a574-42cd0e4b055c/image.png)

Das Board war hilfreich für die direkte Bearbeitung und wurde in folgende Boards unterteilt, dabei wurde sich an die "Scrum" Methodik orientiert:

*   Open
*   Ausstehend
*   in Progress
*   Completed
*   in Prüfung
*   Akzeptiert, Abgelehnt
*   Gesperrt
*   Partner-Community
*   Closed

  

![](https://t43259044.p.clickup-attachments.com/t43259044/add7c708-9bd0-40f3-8ad9-55971520e689/image.png)

Folgend die Listenansichten der Arbeitspakete:

![](https://t43259044.p.clickup-attachments.com/t43259044/dd3a8c44-9f96-4e03-bc8e-f678af229543/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/4903bb76-b43e-4407-92c5-413739e76348/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/4dd2dce5-e4bf-4797-9320-e4ede7ee2bb2/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/6b674801-e1ca-47d5-8e9e-b3f7343ab542/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/aefe13b7-51bd-40c4-81b9-55a28d4d0636/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/b80e6b1f-9e9f-4aab-995b-8f2a20ec4359/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/b665baf7-a81f-4398-9c4e-9326a11e7694/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/6408f740-8211-4b1f-8c21-3d980eb043c1/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/8eac03be-9cb6-4f05-827c-efc47ac89b78/image.png)

  

  

Konkurrenzanalyse:

![](https://t43259044.p.clickup-attachments.com/t43259044/471b15f5-e3a3-4449-8bdf-f31ab641bb48/image.png)

![](https://t43259044.p.clickup-attachments.com/t43259044/66fa7935-65fb-4e23-8dc8-d7e24a9b4af4/image.png)

  

Weiters hatte jeder Task seine eigenen Bereich indem alle nötigen Informationen gespeichert werden konnten, wie folgend zu sehen:

![](https://t43259044.p.clickup-attachments.com/t43259044/1d5c2ff7-f6ca-439a-9552-c7fee53bec25/image.png)