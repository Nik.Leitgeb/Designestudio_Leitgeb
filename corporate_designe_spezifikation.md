# Corporate Designe-Spezifikation

  

Solarhype Farbspektrum: (In Hex Code)

  

Primary White: #FFFEF7

Primary Black: #0C1317

Farbe Solarhype-Blue: #6495ED

Accent Farbe Blue #398AD2

Accent Farbe Yellow: #FFDE73

Aktion Farbe Grau: #454542

Aktion Farbe Dunkelgrau: #3A3936

  

  

Schriftarten:

  

Solarhype Logo Schriftzug: Lato (Semi Bold)

Body Text: Lato (light)

Heading Schriftzug: Poppins (Semi Bold)

Secondary Headline: Bebas Neue Großschreibung)

  

  

Prototypen ersichtlich unter: Private ([https://app.clickup.com/t/2xqhjz4](https://app.clickup.com/t/2xqhjz4))

  

Logo, dass für Solarhype im Design-Prozess erstellt und spezifiziert wurde:

![](https://t43259044.p.clickup-attachments.com/t43259044/ef668152-4ae4-4fa9-9795-b8d01d98f132/Solarhype-Logo.png)

  

![](https://t43259044.p.clickup-attachments.com/t43259044/748babe4-4714-4d51-872b-cae4bb9380b4/Solarhype_Black.png)

  

Icon, dass für Solarhype im Design-Prozess erstellt und spezifiziert wurde:

![](https://t43259044.p.clickup-attachments.com/t43259044/a14e8a1d-5694-438b-9baf-50ee995400e8/Solarhype_icon.png)