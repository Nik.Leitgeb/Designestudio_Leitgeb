# SolarHYPE Webseiten Content

Die Texte wurden von Dominik Leitgeb verfasst und kleine Änderungen sowie Korrekturen die von der Firma SolarHype gewünscht wurden mit einbezogen und darauf abgeändert!

  

  

  

Home || Produkte || Dienstleistungen || Unternehmen ||  Karriere || Kontakt || Newsroom (Neuigkeiten zum Thema, Reverenzen) || Angebot einholen || Pop-up (call to action) || AGB || Datenschutz || Impressum

  

  

Home || [https://solarhype.at/](https://solarhype.at/)
------------------------------------------------------

1.  VIDEO ( Video einer PV Anlage )
    1.  Volle Bildfläche einnehmen und Header Separat
2.  Step by Step (Kurz in 3 Schritten erklärt wie man zum Ziel einer Photovoltaik-Anlage kommt)
    1.  01-Anfrage Schicken
        1.  Sie haben den Wunsch eine Photovoltaikanlage zu besitzen und wollen möglichst rasch ihren eigenen Sonnenstrom produzieren? Kein Problem, bei Solarhype erhalten Sie Ihre passende, optimal auf Ihre Anforderungen abgestimmte Photovoltaikanlage.
    2.  02-Rundum Sorglospaket Buchen
        1.  Ihnen fehlt die Zeit oder Sie verfügen nicht über das notwendige Fachwissen, um Ihr Projekt zu organisieren und durchzuführen? Kein Problem, wir haben für Sie ein "Rundum-Sorglospaket" geschnürt, bei dem Sie sich entspannt zurücklehnen können. Wir übernehmen für Sie alle Schritte, die Ihr geplantes Projekt möglich machen!
    3.  Anlage in Betrieb nehmen
        1.  Das Schönste in Zeiten steigender Energiekosten ist der Moment, in dem Sie Ihre eigene Energie produzieren können. Wir machen es für Sie möglich und freuen uns mit Ihnen auf den Moment, Ihre Anlage in Betrieb zu nehmen!
3.  "WHY-Heading" und Deskription
    1.  „Ihr Energieoptimierer für ihre Unabhängigkeit“ - Spezialisiert auf Photovoltaikanlagen, Wechselrichter, Paneele, Batteriespeicher, Montagesysteme und vieles mehr.
        1.  Sie möchten eine komplette Photovoltaikanlage? [Angebot einholen](https://solarhype.at/angebot-einholen/)
        2.  oder einzelne Komponenten wie Module, Wechselrichter, Speicher, Montagesysteme und Zubehör. [Zum Shop](https://shop.solarhype.at/)
        3.  oder Sie wollen Partner von Solarhype werden oder haben eine individuelle Frage zu unseren Angeboten? [Kontakt](https://solarhype.at/kontakt/)
4.  Link zum Unternehmen:
    1.  Sie wollen wissen wer wir sind?
        1.  Die nachhaltige Energiewende ist in unseren Köpfen. Wir wollen die Nachhaltigkeit vorantreiben und damit Energie und Kosten für Sie sparen. Damit Sie in die nachhaltige Energiezukunft investieren können. Wir zeichnen uns dadurch aus, dass wir nachhaltig denken und handeln. Wir unterstützen Sie dabei, in Zukunft nachhaltiger zu leben. Als dynamisches und mutiges Team stellen wir uns den Herausforderungen der zukünftigen Energieversorgung und Energiesicherheit,  mit innovativen Lösungen.
        2.  [Mehr erfahren](https://solarhype.at/unternehmen/)
5.  UNSERE SERVICES ( Produkte und Dienstleistungen - Beschreibung und Link zur jeweiligen Seite)
    1.  Was wir für Sie tun können
        1.  Möchten Sie wissen, welche unserer Dienstleistungen unsere Kunden am häufigsten in Anspruch nehmen? Informieren Sie sich über unsere beliebtesten Optionen oder sehen Sie sich unsere vollständige Liste der Produkte und Dienstleistungen  an.
            1.  Vor-Ort-Abholung
                1.  Wir führen alles rundum Photovoltaik-Anlagen und vieles mehr! In unserem Lager in Weißkirchen können Sie alle Produkte einfach und bequem jederzeit abholen oder sie lassen es sich vor die Haustüre liefern.
            2.  Unsere Produkte
                1.  Photovoltaik Paneele, Wechselrichter, Speicher- und Montagesysteme sowie Smartmeter und jegliches Zubehör. Unsere Produktpalette erweitert sich ständig um all Ihre Bedürfnisse Händeln zu können.
            3.  Berechnung Der Wirtschaftlichkeit Und Energieoptimierung
                1.  Wir unterstützen Sie dabei, in einem immer komplexer werdenden Energiemarkt die richtigen Entscheidungen zu treffen und damit Kosten zu sparen, Erträge zu steigern und eigene Ressourcen zu schonen. So schaffen wir Freiräume und Sie können sich auf Ihr Kerngeschäft konzentrieren.
            4.  Planung Und Auslegung
                1.  Nach einer ausführlichen Beratung und der Planung des Konzeptes können wir auch die Umsetzung für Sie übernehmen. Dabei ist es uns wichtig, Ihr geplantes Projekt möglich zu machen. Hierfür bieten wir Ihnen eine Komplettlösung in Form unseres "Rundum-Sorglospakets".
            5.  [Unsere Produkte](https://solarhype.at/produkte/) | [Direkt zum Shop](https://shop.solarhype.at/) | [Unsere Dienstleistungen](https://solarhype.at/dienstleistungen/)
    2.  Wir erfüllen jedes Kriterium für Sie
        1.  Wir erfüllen jedes Kriterium für Sie
            1.  Wir erfüllen jedes Kriterium für Sie
                1.  Holen Sie sich einen Preis, der Ihren Bedürfnissen entspricht. Was auch immer Sie brauchen, wir können es realisieren. Wählen Sie das "Rundum - Sorglospaket" oder bringen Sie Ihr Fachwissen ein und beauftragen Sie uns nur für die Teile, die Sie für Ihr Projekt benötigen.
            2.  Betreuung In Echtzeit
                1.  Sie können sich jederzeit an unser Team wenden, wenn Sie Informationen über unsere Produkte und Dienstleistungen benötigen.
            3.  Die Auslieferung Erfolgt Rasch
                1.  Wir installieren Ihre Photovoltaikanlagen in Rekordzeit. Wir setzen alles in Bewegung, um Ihre Ziele so schnell wie möglich zu erreichen. Darüber hinaus werden Ihre bestellten Pakete immer schnell und pünktlich zugestellt.
            4.  Wir Leben Nachhaltigkeit
                1.  Wir tun alles dafür, dass Nachhaltigkeit eine Selbstverständlichkeit ist. Unsere Produkte werden nach diesen Kriterien für Sie ausgewählt. Zum einen Lösungen mit erneuerbaren Energien und zum anderen zeigt Ihnen unsere Energieoptimierung, wie Sie Energie sparen können.
6.  SIE WOLLEN MEHR ÜBER UNSERE UNTERNEHMUNGEN ERFAHEN (NEWSROOM)
    1.  Was Sie sehen, ist was Sie bekommen
        1.  Genießen Sie einen pünktlichen und effizienten Service, der Sie in Ihrer Entscheidung für Solarhype bestärkt. Erwarten Sie, dass Sie immer 100% zufrieden sind. Informieren Sie sich in unserem Newsroom über unsere Projekte und Neuigkeiten zum Thema.
        2.  [Zum Newsroom](https://solarhype.at/newsroom/) --(UNSERE REFERENZEN (link newsroom))
7.  Kundenempfehlungen:
    1.  Warum unsere Kunden Solarhype empfehlen
        1.  Wir stellen sicher, dass jeder Kunde zu 100 % mit seiner Erfahrung und seinem Service zufrieden ist. Keine Ausnahmen.
            1.  "Schnelle Reaktionszeiten, toller Service und Support ... Solarhype ist unser bevorzugter Photovoltaik-Partner." -HANNES MATT
            2.  "Es ist sehr hilfreich, Informationen und Hilfe bei der Planung unserer Photovoltaikanlage in Echtzeit zu erhalten. Ich empfehle die Dienste von Solarhype immer weiter." - ELENA MAIR
            3.  "Ich bin immer wieder froh, dass ich mich für Solarhype entschieden habe. Toller Kundenservice und superschnelle Lieferungen!" - MICHAELA WISEMANN
            4.  "Solarhype ist ein Win-Win-Service für Photovoltaikanlagen. Toller Support, Effizienz und super budgetfreundlich." - PHILLIPP NEUMANN
8.  Feedback:
    1.  Wir von SolarHype GmbH freuen uns auf Ihr Feedback. Veröffentlichen Sie doch eine Rezension in unserem Profil.
        1.  [Zur Rezesion](https://g.page/r/CcooSuxeT2BzEAI/review)

  

Dienstleistungen [https://solarhype.at/dienstleistungen/](https://solarhype.at/dienstleistungen/)
-------------------------------------------------------------------------------------------------

  

1.  ONE STOP (Beschreibung alles für ein rundum sorglos Paket PV-Komplettanlagen B2B und B2C) \[Idee Kunde (Komplettanlage  Beratung für die Ideenumsetzung) |Planung & Individuelles Angebot  (Individuelle Planung) | Förderung und Zählpunktbeantragung | Wirtschaftlichkeitsberechnung |Montage und Inbetriebnahme | Service Leistungen\]
    1.  Rundum Sorglospaket
        1.  Sie wollen ein „Rundum-Sorglospaket“ für Ihre komplette Photovoltaikanlage? Dann ist Solarhype der richtige Partner für Sie! Wir bieten Ihnen mit unserem „Rundum-Sorglospaket“ eine komplette Betreuung Ihres Photovoltaik-Projektes von der Photovoltaik-Idee über die Planung, Förderanträge, Zählpunktanträge, Wirtschaftlichkeitsberechnung bis hin zur Installation und Inbetriebnahme. Ein kompetenter und erfahrener Ansprechpartner für alle Ihre Fragen und Anliegen sowie zusätzliche Optionen, die Ihnen einen schnellen und problemlosen Start Ihrer Photovoltaikanlage ermöglichen.
            1.  Idee Kunde
                1.  Sie haben den Wunsch, eine Photovoltaikanlage zu besitzen und wollen diese möglichst schnell in eine laufende Produktion überführen? Kein Problem, bei Solarhype erhalten Sie Ihre passende Photovoltaikanlage, optimal angepasst an Ihre Bedürfnisse. Fordern Sie gleich ein Angebot an!
            2.  Individuelle Planung und Angebot
                1.  Wir übernehmen für Sie die Planung und Gestaltung Ihres Projektes nach den höchsten Standards in Österreich. Wir werden Ihnen so schnell wie möglich ein Angebot für Ihr Projekt erstellen
            3.  Förderung und Zählpunkt Beantragung
                1.  Sie haben keine Zeit oder wissen nicht, wo Sie mit der Organisation rund um Ihr Photovoltaik-Projekt anfangen sollen. Wir erledigen auch die bürokratischen Meilensteine für Sie. Und beantragen gerne einen Zuschuss für Ihr Projekt.
            4.  Berechnung der Wirtschaftlichkeit
                1.  Lohnt sich nun eine Investition überhaupt? Diese Frage stellen sich zur Zeit viele. Wir beantworten diese Frage gerne auf Ihr individuelles Projekt und realisieren das bestmögliche Ergebnis!
            5.  Montage und Inbetriebnahme
                1.  Sie sind ein Mensch der Tat und realisieren Ihre Montage selbst? Perfekt, wenn nicht, übernehmen wir gerne die Montage für Sie bis zur Inbetriebnahme. Lehnen Sie sich einfach zurück und beobachten Sie die Fertigstellung.
            6.  Geniessen
                1.  Jetzt ist es soweit, Sie sind Besitzer:in einer eigenen Photovoltaikanlage! Wir wünschen Ihnen damit viel Freunde und senden Ihnen sonnige Grüße. Wir danken Ihnen für Ihre Zusammenarbeit.
2.  Energieoptimierung (Beschreibung)
    1.  Themen wie Energiewende und dezentrale Energieversorgung haben zu einem komplexen Marktumfeld geführt. Bei gleichzeitig begrenzten Ressourcen wird die ganzheitliche Optimierung von Energiesystemen damit zum entscheidenden Erfolgsfaktor. Wir unterstützen Sie dabei, in einem immer komplexer werdenden Energiemarkt die richtigen Entscheidungen zu treffen und damit Kosten zu sparen, Erträge zu steigern und eigene Ressourcen zu schonen. So schaffen wir Freiräume und Sie können sich auf Ihr Kerngeschäft konzentrieren.
        1.  Was wir für Unternehmen anbieten
            *   Energieflussanalyse
            *   Steigerung der Energieeffizienz
            *   Wirtschaftlichkeitsberechnung von Investitionen in innovative und erneuerbare Energien
            *   Energiekostenminimierung zur Steigerung der Wettbewerbsfähigkeit
        2.  Was wir für Private anbieten
        3.  Energiekosten-Check
        4.  Analyse der Energieverbräuche
        5.  Identifizierung von Einsparpotenzialen
        6.  Rentabilitätsanalyse von Investitionen in innovative und erneuerbare Energielösungen

  

Produkte|[https://solarhype.at/produkte/](https://solarhype.at/produkte/)
-------------------------------------------------------------------------

  

1.  Wählen Sie das Produkt, das für Sie geeignet ist.
    1.  Sie sehen auf dieser Seite unsere Standardprodukte.  Um alle aktuellen Produkte zu sehen schauen Sie in unserem Shop vorbei! - [Zum Shop](https://shop.solarhype.at/)
2.  Unsere Produkte
    1.  Wir führen alles rundum Photovoltaik-Anlagen und vieles mehr! In unserem Lager in Weißkirchen können Sie alle Produkte einfach und bequem jederzeit abholen oder sie lassen es sich vor die Haustüre liefern.
    2.  Photovoltaik Paneele, Wechselrichter, Speicher- und Montagesysteme sowie Smartmeter und jegliches Zubehör. Unsere Produktpalette erweitert sich ständig um all Ihre Bedürfnisse Händeln zu können.
        1.  1.  Photovoltaik Paneel
                1.  Wir verwenden je nach Verfügbarkeit PV-Module von mehreren internationalen Spitzenherstellern. Es handelt sich um Halbzellenmodule der neueste Generation von Hochleistungsmodulen mit den folgenden Eckdaten:
                2.  Leistung: 200-500 Watt
                3.  Antireflexionsbeschichtung
                4.  Umgebungstemperatur: -40° C bis +85° C
                5.  Erfüllt alle wichtigen Normen
                6.  12 Jahre Produktgarantie des Herstellers
                7.  25 Jahre Garantie auf die lineare Leistungsabgabe
            2.  Wechselrichter
                1.  Unsere Wechselrichter in der Produktpalette sind in den unterschiedlichsten Ausführungen und von internationalen Top-Herstellern erhältlich.
                2.  3-phasiger Wechselrichter von 5 bis 200kWp
                3.  2-4 unabhängige MPP Tracker
                4.  Installation über FusionSolarApp
                5.  Wirkungsgrad bis zu 98.5%
                6.  Kommunikation per WLAN/Ethernet\* und 4G\*
                7.  AC und DC Überspannungsschutz
                8.  25 Jahre Garantie auf die lineare Leistungsabgabe
            3.  Speichersysteme
                1.  Unsere Batteriespeicher im Sortiment sind in einer Vielzahl von Ausführungen erhältlich. Sie sind notstromfähig, wenn eine Backup-Box installiert ist. Darüber hinaus sind sie aufgrund ihres modularen Aufbaus leicht erweiterbar.
                2.  Nutzbare Energie: 5 - 30 kWh
                3.  Max. Entladeleistung: 3,5 - 7 kW, 10s
                4.  Herstellergarantie: 10 Jahre
                5.  Zelltechnologie: Lithium-Eisenphosphat
                6.  Betriebstemperatur: -10°C~ bereich: +55°C
            4.  Montagesysteme
                1.  Für PV-Anlagen auf Trapez- und Welldächern sowie Schrägdächern, Giebeldächern oder Flachdächern bietet die SolarHYPE GmbH ein spezielles Sortiment an Solar-Montagekomponenten. Sie erhalten hohe Qualität kombiniert mit einfacher Montage und statischer Sicherheit.
                2.  Optimiert für alle gängigen Dacheindeckungen
                3.  Einfache und schnelle Montage
                4.  Sehr gutes Preis-/Leistungsverhältnis
                5.  Korrosionsbeständig durch Edelstahlverarbeitung
3.  FAQ Fequently asked Questions
    1.  [**Können wir liefern, und wie lange wird es dauern?**](https://solarhype.at/produkte/)
        1.  Ja, wir machen nur Versprechungen, die wir auch halten können! Unsere Lager werden ständig von unseren Partnern aufgefüllt. Rechnen Sie mit ca. 3 Monaten von der Vertragsunterzeichnung bis zur Inbetriebnahme.
    2.  [**Können Sie mich vor Ort beraten?**](https://solarhype.at/produkte/)
        1.  Ja, sehr gerne, wenn Sie den persönlichen Kontakt bevorzugen! Unsere Geschäftsidee ist es, den kompletten Planungsprozess Ihrer PV-Anlage mit Ihnen online und per Telefon durchzuführen. Das spart uns und Ihnen viel Zeit und vor allem unnötige Kosten, die wir in ein tolles Produkt zu fairen Preisen stecken.
    3.  [**Brauche ich für den Betrieb einer Photovoltaikanlage einen Smart Meter?**](https://solarhype.at/produkte/)
        1.  Ja, denn ein Smart Meter misst nicht nur Ihren Stromverbrauch aus dem örtlichen Stromnetz, sondern auch Ihre Stromeinspeisung in das örtliche Netz. Der Einbau des Smart Meters erfolgt durch den regionalen Stromnetzbetreiber. Dies geschieht im Rahmen unseres „Rundum-Sorglospakets“- wir übernehmen gerne die Kommunikation für Sie.
    4.  [**Was beinhaltet das "Rundum-Sorglospakets" von SolarHYPE genau?**](https://solarhype.at/produkte/)
        1.  SolarHYPE bietet Ihnen eine fertige Anlage, bei der Sie sich nur noch zurücklehnen und die Fertigstellung beobachten müssen. Wir übernehmen für Sie die gesamte Planung, Installation, Montage und Inbetriebnahme der Anlage.
        2.  **Das Angebot umfasst:**
            1.  Hochwertige Systemtechnik mit hochwertigen Photovoltaik-Modulen und innovativen Mikro-Wechselrichtern, die perfekt auf Ihre Bedürfnisse abgestimmt sind.
            2.  Kommunikationsgateway und entsprechende Anschlüsse
            3.  Komplette Planung und technische Prüfung vor der Umsetzung inkl. Wirtschaftlichkeitsberechnung
            4.  Installation der Anlage inklusive notwendiger Anpassungen an die Hauselektrik gemäß Projektvorschlag
            5.  Fachgerechte Installation auf gängigen Dachformen wie Giebel-, Pult-, Walm- oder Flachdächern inklusive Unterkonstruktion und Installationsmaterial
            6.  Anmeldung beim örtlichen Verteilnetzbetreiber und fachgerechte Inbetriebnahme Ihrer PV-Anlage
            7.  Wir beantragen auch Ihre Fördermittel, um einen möglichen Zuschuss für Ihr Projekt zu erhalten.
    5.  [**Was genau ist ein Mikro-Wechselrichter, welcher wird verwendet, was bringt er?**](https://solarhype.at/produkte/)
        1.  Ein Mikro-Wechselrichter, d. h. ein Konverter, der Gleichstrom in Wechselstrom umwandelt, den Sie in Ihrem Haushalt nutzen oder ins Netz einspeisen können. Fällt zum Beispiel ein Solarmodul aus, fließt der Strom weiter. Wir verwenden standardmäßig Enphase IQ7plus Mikro-Wechselrichter.
        2.  **Diese bieten u.a. folgende Vorteile:**
        3.  Flexibel erweiterbar: Zusätzliche PV-Module können jederzeit flexibel hinzugefügt werden. Auch Batterielösungen können problemlos nachgerüstet werden.
        4.  Zuverlässiges System: Durch das parallel geschaltete Mikro-Wechselrichterkonzept kann auch bei Ausfall eines Wechselrichters weiterhin Strom fließen.
        5.  Stromerzeugung auch bei tiefstehender Sonne: Die integrierte Burst-Mode-Technologie sorgt dafür, dass auch bei schwachen Lichtverhältnissen (z.B. Abschattung durch Wolken) Strom erzeugt werden kann.
        6.  Mehr Brandsicherheit: Da das Mikro-Wechselrichtersystem keinen Hochspannungsgleichstrom führt, können Lichtbogenbrände vermieden werden.
    6.  [**Wie sieht es mit der Wirtschaftlichkeit einer PV-Anlage aus?**](https://solarhype.at/produkte/)
        1.  Eine PV-Anlage lohnt sich für fast alle bewohnten Immobilien. Zum einen können Sie den im Haus erzeugten Solarstrom selbst verbrauchen, zum anderen können Sie ihn ins Netz einspeisen und von der attraktiven Vergütung bei dem derzeit hohen Strompreis profitieren. Wir können Ihnen eine Wirtschaftlichkeitsberechnung für Ihr individuelles Projekt anbieten.
    7.  [**Bietet ihr auch ein Balkonkraftwerk an?**](https://solarhype.at/produkte/)
        1.  Ja, wir bieten Ihnen die Möglichkeit, kleine Systeme für Balkon und Garten zu installieren. Bitte besuchen Sie dafür unseren Online Shop oder kontaktieren Sie uns über unser Kontaktformular.
    8.  [**Wie sieht es mit dem Recycling von technischen Teilen wie Modulen und Wechselrichtern in ein paar Jahren aus?**](https://solarhype.at/produkte/)
        1.  Der sorgfältige Umgang mit allen Materialien liegt uns besonders am Herzen. Deshalb achten wir auch darauf, dass wir alles recyceln, was möglich ist. Glücklicherweise ist die Photovoltaik keine ganz neue Technologie, weshalb es bereits eine etablierte Recyclingindustrie für die Wiederverwendung gibt.

  

Unternehmen [https://solarhype.at/unternehmen/](https://solarhype.at/unternehmen/)
----------------------------------------------------------------------------------

  

1.  Über das Unternehmen
    1.  Wir sind dafür da, Energie und Kosten für Sie zu sparen und Ihnen eine Investition in die Zukunft zu ermöglichen. Für eine nachhaltige Zukunft setzen wir uns ein und unterstützen Sie dabei, das Gleiche zu tun. Als dynamisches und mutiges Team stellen wir uns den Herausforderungen der zukünftigen Energieversorgung mit innovativen Lösungen.
2.  Video:
3.  Solarhype und wer wir sind
    1.  Team Mit Einer Sonnigen Mission
        1.  Unsere Vision ist es, Menschen in die Lage zu versetzen, ihr eigenes Haus mit Solarenergie zu versorgen. Unsere Mission: die Photovoltaik so einfach und erschwinglich zu machen, dass möglichst viele Menschen in diese Technologie investieren können. Daran arbeiten wir Tag für Tag mit den besten Köpfen und der neuesten Technologie. Gemeinsam mit Ihnen schaffen wir die Energiewende – bei Ihnen zu Hause, in Österreich und in Europa. Mit Photovoltaik auf jedem Haus wollen wir den Weg in eine emissionsfreie Energiezukunft beschleunigen.
4.  Impressionen
    1.  Für Eine Lebenswerte Zukunft
        1.  Lassen Sie uns gemeinsam einen Beitrag zur Zukunft leisten, indem wir das Richtige tun. Wir schaffen einen Ort, an dem die Zukunft durch Nachhaltigkeit und erneuerbare Energien lebenswert bleibt.
5.  WERTE HINTER DENEN WIR STEHEN (Werte und Beschreibung dazu)
    1.  1.  1.  Maßgebend
                1.  Dieser Wert lässt uns zu allen erfolgreichen und umweltfreundlichen Unternehmen aufblicken. Wir haben uns zum Ziel gesetzt, von ihnen zu lernen und ein Trendsetter im Bereich der erneuerbaren Energien zu werden.
            2.  Handschlag Qualität
                1.  Wir haben festgestellt, dass die Handschlag Qualität im Geschäftsleben immer mehr in den Hintergrund tritt oder sogar ganz verloren geht. Wir beleben dies wieder, und stellen Vertrauen, Ehrlichkeit und Anstand gegenüber den Menschen in den Mittelpunkt.
            3.  Bewegend
                1.  Wir sind bereit für eine spannende Zukunft, in der wir unsere Umwelt mitgestalten können. Dieser Wert symbolisiert für uns, dass wir die Menschen rund um unser Projekt einbinden wollen, um eine lebenswerte Zukunft zu gestalten.
            4.  Zielstrebig
                1.  Jedes Ziel, das erreicht werden will, kann kurz oder langweilig sein. Unsere Motivation, in diesem Bereich zu arbeiten, garantiert eine spannende und vor allem schnelle Umsetzung unserer Ziele und ihrer Projekte.
            5.  Mutig
                1.  Mut ist die schönste Erfahrung, wenn man damit Erfolg hat, und er macht jeden Weg lebenswert. Mutig sein heißt für uns, laut zu sein, aufzufallen und gemeinsam die richtigen Entscheidungen zu treffen.
            6.  Effizient
                1.  Nichts stört uns mehr als ineffiziente Systeme, deshalb haben wir uns der Energieoptimierung verschrieben. Energie, die nicht verbraucht werden muss, muss auch nicht erzeugt werden. Wir leben und lieben effiziente Systeme für uns und Sie.
6.  Unser Team
    1.  1.  1.  Stefan Fürlinger
                1.  CEO
                2.  [Linkedin](https://www.linkedin.com/in/stefan-fuerlinger/)
            2.  August Fürlinger
                1.  ENERGIEOPTIMIERER
            3.  Felix Brückl
                1.  CTO
                2.  [Linkedin](https://www.linkedin.com/in/felix-br%C3%BCckl-98807a206/)
            4.  Jasmin Gruber
                1.  OFFICE
                2.  [Linkedin](https://www.linkedin.com/in/jasmin-gruber-/)
            5.  Leitgeb Dominik
                1.  CHANGE MANAGEMENT CONSULTANT
                2.  [Linkedin](http://www.linkedin.com/in/leitgeb-dominik-change-management-consultant)
            6.  Gerti Fürlinger
                1.  BUCHHALTUNG
7.  Join Us Today
    1.  Let´S Start Working Together
        1.  Wir sind ein motiviertes, dynamisches und vor allem humorvolles Team. Wir sind stolz darauf, ein Unternehmen mit einer echten Aufgabe aufzubauen und einen Beitrag zur Gesellschaft als Ganzes zu leisten.
        2.  Wenn auch Sie auf der Suche nach einem Job mit Sinn und Antrieb sind, freuen wir uns auf Ihre Bewerbung.
        3.  [Bewerben](https://solarhype.at/karriere/)
    2.  Stellenangebote | Mitarbeiter werden
        1.  1.  Lagerlogistik (M/W/D)
                1.  In unserem Tätigkeitsbereich werden viele Produkte umgeschlagen. Wir suchen Sie als Unterstützung für unsere Lagerarbeit.
                2.  [Bewerben](https://solarhype.at/karriere/)
            2.  Assistent/In Office (M/W/D)
                1.  Jedes Unternehmen braucht Organisation und Planung. Wir suchen Sie zur Unterstützung in unserem Office.
                2.  [Bewerben](https://solarhype.at/karriere/)
            3.  PV-Monteur/In (M/W/D)
                1.  Die PV-Branche boomt. Wir suchen Sie als Unterstützung für unsere Installation und Montage von PV-Anlagen.
                2.  [Bewerben](https://solarhype.at/karriere/)
            4.  Projektplanung (M/W/D)
                1.  Jedes Projekt ist anders! Sie suchen Abwechslung und wollen in der Planung und Auslegung arbeiten, dann sind Sie genau richtig
                2.  [Bewerben](https://solarhype.at/karriere/)

  

Karriere || [https://solarhype.at/karriere/](https://solarhype.at/karriere/)
----------------------------------------------------------------------------

Kontakt || [https://solarhype.at/kontakt/](https://solarhype.at/kontakt/)
-------------------------------------------------------------------------

  

1.  Kontaktieren Sie uns
    1.  Schicken Sie uns eine Nachricht, wenn Sie etwas über die Solarhype-Dienstleistungen oder Produkte wissen möchten, und wir werden uns umgehend bei Ihnen melden
2.  Kontaktformular
    1.  Vorname
    2.  Familienname
    3.  Telefonnummer
    4.  Email Adresse
    5.  Anfrage
        1.  Wir kümmern uns gerne um Ihr Anliegen! Was können wir für Sie tun?
    6.  Absenden
3.  Oder treten Sie mit uns in Verbindung
    1.  Telefon
        1.  +43 (0) 677 647 65 461
    2.  Mailen Sie Uns
        1.  [office@solarhype.at](mailto:office@solarhype.at)
    3.  Öffnungszeit
        1.  Mo -Do:
            1.  8:00 - 12:00
            2.  13:00 - 17:00
        2.  Fr:
            1.  8:00 - 13:00
    4.  Besuchen Sie Uns
        1.  Fasanenweg 4
        2.  Weißkirchen a. d. Traun,
        3.  AT-4616
        4.  Österreich

  

  

  

  

  

Newsroom || [https://solarhype.at/newsroom/](https://solarhype.at/newsroom/)
----------------------------------------------------------------------------

Kategorien erstellen und auswählbar machen | Suchfunktion |

Beiträge werden dann in Zukunft verfasst (1 News Artikel und 3 Referenz Beiträge inkludiert))

  

1.  Newsroom
    1.  Erhalten Sie einen Einblick in unsere Arbeit und erfahren Sie mehr über das SolarHYPE-Kundenerlebnis hinter den Kulissen.
2.  Suchformular
3.  Kategorie Auswahlfenster
4.  Beitrags-Archiv
    1.  Photovoltaik Wissen
        1.  [Alles Über Photovoltaik Und Ihre Vor- Und Nachteile](https://solarhype.at/was-sie-ueber-photovoltaik-anlagen-wissen-sollen/)
    2.  Referenzen
        1.  [Photovoltaik Anlage 10kWp Inkl. Batteriespeicher](https://solarhype.at/photovoltaik-anlage-10kwp/)
        2.  [Sparmarkt Linz – 50 KWp Photovoltaik](https://solarhype.at/sparmarkt-linz-50-kwp-photovoltaik/)
        3.  [10 KWp Anlage Mit 30 Stück Modulen](https://solarhype.at/10-kwp-anlage-mit-30-stueck-modulen/)

  

Angebot einholen || [https://solarhype.at/angebot-einholen/](https://solarhype.at/angebot-einholen/)
----------------------------------------------------------------------------------------------------

*   Welche Zentralen fragen sind wichtig für ein Angebot?
    *   Name
    *   E-Mail
    *   Tel. Nr.
    *   Anlagentyp
    *   Anlagengröße
    *   Dach Art

  

1.  Angebot einholen
    1.  Um einen Kostenvoranschlag für Ihre Photovoltaik-Anlage zu erhalten, füllen Sie einfach diesen vierstufigen Prozess aus, in dem Sie Ihre persönlichen Daten und Ihre Projektdaten angeben.
    2.  Bitte beachten Sie, dass sich dieses Angebot ausschließlich auf Photovoltaikanlagen bezieht. Wenn Sie andere Angebote erhalten möchten, kontaktieren Sie uns bitte über unser Kontaktformular. [Kontakt](https://solarhype.at/kontakt/)
    3.  Wenn Sie ausschließlich Materialien benötigen, können Sie diese auch direkt in unserem Shop erwerben. [Zum Shop](https://shop.solarhype.at/)
2.  Angebotsformular
    1.  1.  1.  Vorname § Nachname
            2.  Email
            3.  Mobiltelefonnummer
            4.  Adresse
            5.  Angebotsleistung (BITTE ANKREUZEN)
                1.  Material und Planung
                2.  Material und Montage
                3.  Komplettlösung (Material, Montage und Elektriker)
            6.  Ihr Stromverbrauch (Standard Einfamilienhaus 4000kWh/Jahr)
            7.  Gewünschte Anlagenleistung (Richtwert: Pro 1000 KWh/Jahr Verbrauch, 2 KWp Anlagenleistung)
            8.  Dacheindeckung (BITTE AUSWÄHLEN)
            9.  Einspeisezählpunkt (BITTE AUSWÄHLEN)
            10.  Inventarnummer, Zählpunktnummer, Stromnetzbetreiber Und Kundennummer-Stromnetzbetreiber NUR AUSFÜLLEN WENN Einspeisezählpunkt NICHT VORHANDEN
                1.  Inventarnummer (Am Stromzähler)
                2.  Zählpunktnummer (Am Stromzähler)
                3.  Stromnetzbetreiber
                4.  Kundennummer-Stromnetzbetreiber
            11.  Strom Speicher
                1.  Ja gewünscht
                2.  Nein nicht gewünscht
            12.  Speichergröße (Richtwert: Pro 1000kWh/Jahr Verbrauch, 2kWh Speicher)
                1.  5 kWh - 20kWh
                2.  nicht gewünscht
            13.  Sie Haben Projektspezifische Dateien?
            14.  Datenschutzerklärung 
                1.  Hiermit bestätige ich den Datenschutz von Solarhype ([Link zum Datenschutz](https://solarhype.at/datenschutz/))
            15.  Angebot einholen

  

  

  

###   

### Folge Uns:

##### Folge uns auf

[Linkedin](http://linkedin.com/company/solarhype/)

© 2023, Solarhype GmbH, Alle Rechte vorbehalten

  

### Pop-up (call to action)

  

Die Sonne scheint und Sie erhalten
----------------------------------

3% Rabatt
---------

für Ihre erste Bestellung in unserem Online-Shop.
-------------------------------------------------

Der Online Shop kommt bald!!! Wir schicken Ihnen den 3% Gutschein, sobald er online ist.  Sichern Sie sich schon jetzt ihren Rabatt.

  

  

  

### Datenschutz

[datenschutz.docx](https://t43259044.p.clickup-attachments.com/t43259044/77a88cda-2634-4860-9dbb-fb0722edde1e/datenschutz.docx)

### AGB

[AGB.docx](https://t43259044.p.clickup-attachments.com/t43259044/ed2eb57b-7df9-4cb7-b5f9-51d64ac5e463/AGB.docx)

  

### Impressum

SolarHype GmbH

Fasanenweg 4

A-4616 Weißkirchen

UID: ATU78512469

Email: [office@solarhype.at](mailto:office@solarhype.at)

Telefon: +43 677 647 65 461

Geschäftsführung/Juristische Person: Stefan Fürlinger, Felix Brückl

Unternehmensgegenstand:

Handelsgewerbe,

Montage von Solar- und Photovoltaikmodulen ohne Anschlussarbeiten

Firmenbuchnummer: FN587292s

Firmenbuchgericht: Wels

Firmensitz: 4616 Weißkirchen

Mitglied bei: WKO

Aufsichtsbehörde/Gewerbebehörde: Bezirkshauptmannschaft Wels-Land

Verleihungsstaat: Österreich

  

  

  

  

  

  

  

  

  

  

  

  

  

  

OPTIONALE FELDER DIE VORERST NICHT UMGESETZT WURDEN:

  

*   WIR STEHEN FÜR EINE ENERGIEOPTIMIERTE ZUKUNFT DIE NACHHALTIGKEIT ALS SELBSTVERSTÄNDLICH ANSIEHT (Link zu Umweltzertifikate, CO2 Bilanzen und Nachhaltigkeitsunternehmungen z.B. AGÖ) BILD GRÜNES MOS UND SOLARHYPE LOGO
*   ENERGYCONTRACTING (Beschreibung) - Felder:
    *   Komplettlösungen Produkte
    *   Energieoptimierung und Beratung
    *   Photovoltaic Contracting
*